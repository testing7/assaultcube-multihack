﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Diagnostics;
using System.Runtime.InteropServices;
using BasicInjector.Native;
using System.Windows.Forms;

namespace BasicInjector
{
    public enum EProcessAddressSpace
    {
        x86,
        x64
    }

    public static class ProcessUtils
    {
        public static IEnumerable<string> EnumerateModules(Process proc)
        {
#if WIN64
            uint snapshotFlags = (uint)Imports.SnapshotFlags.Module | (uint)Imports.SnapshotFlags.Module32;
#elif WIN32
            uint snapshotFlags = (uint)Imports.SnapshotFlags.Module;
#endif
            IntPtr snapshotHandle = Imports.CreateToolhelp32Snapshot(snapshotFlags, (uint)proc.Id);
            if (snapshotHandle.ToInt64() <= 0)
            {
                throw new Exception("Could not get snapshot of process '" + proc.ProcessName + "': " + Marshal.GetLastWin32Error().ToString());
            }
            Imports.MODULEENTRY32 modEntry = new Imports.MODULEENTRY32() { dwSize = (uint)Marshal.SizeOf(typeof(Imports.MODULEENTRY32)) };
            if (!Imports.Module32First(snapshotHandle, ref modEntry))
            {
                Imports.CloseHandle(snapshotHandle);
                throw new Exception("Could not walk module list of process '" + proc.ProcessName + "': " + Marshal.GetLastWin32Error().ToString());
            }
            do
            {
                /*
                // TO-DO:
                // NOTE:
                // debug only!
                if (modEntry.szModule.ToLower().Contains("kernel"))
                {
                    MessageBox.Show(modEntry.szModule.ToLower() + " has base addr " + modEntry.modBaseAddr.ToString("X2"));
                }
                */
                yield return modEntry.szModule.ToLower();
            }
            while (Imports.Module32Next(snapshotHandle, ref modEntry));

            Imports.CloseHandle(snapshotHandle);
        }

        public static bool IsMonoLoaded(Process proc)
        {
            // TO-DO:
            // might need better detection here
            // can foresee false positives causing major problems
            foreach (string moduleName in EnumerateModules(proc))
            {
                if ((moduleName.Contains("mono")) && (moduleName.EndsWith(".dll")))
                    return true;
            }
            return false;
        }

        public static bool IsNetLoaded(Process proc)
        {
            // TO-DO:
            // might need better detection here
            // can foresee false positives causing major problems
            foreach (string moduleName in EnumerateModules(proc))
            {
                if ((moduleName.Contains("clr.dll")) || (moduleName.Contains("mscoree")))
                    return true;
            }
            return false;
        }

        public static bool Is64BitProcess(Process proc)
        {
            if (!Environment.Is64BitOperatingSystem)
                return false;
            IntPtr procHandle = Imports.OpenProcess((Imports.PROCESS_CREATE_THREAD | Imports.PROCESS_QUERY_INFORMATION | Imports.PROCESS_VM_OPERATION | Imports.PROCESS_VM_WRITE | Imports.PROCESS_VM_READ), 1, (uint)proc.Id);
            if (procHandle == (IntPtr)0)
            {
                throw new Exception("Could not get handle to target process: " + Marshal.GetLastWin32Error().ToString());
            }
            bool isWoW64 = false;
			            // okay, wait, this is the most misleading function name ever
            // Documentation:
            // A pointer to a value that is set to:
            // TRUE if the process is running under WOW64 on an Intel64 or x64 processor.
            // FALSE If the process is running under 32-bit Windows. 
            //      * Accounted for. If OS is NOT 64bit then no process can be 64bit.
            // FALSE If the process is a 32-bit application running under 64-bit Windows 10 on ARM. 
            //      * Only if it's on ARM? Okay, not relevant here, then
            // FALSE If the process is a 64-bit application running under 64-bit Windows
            //
            // so, Is64BitProcess = !isWoW64 since we exclude the possibility of it being 32bit windows above
            if (!Imports.IsWow64Process(procHandle, out isWoW64))
            {
                Imports.CloseHandle(procHandle);
                throw new Exception("Call to native IsWoW64Process failed: " + Marshal.GetLastWin32Error().ToString());
            }

            Imports.CloseHandle(procHandle);
            return !isWoW64;
        }
    }
}
