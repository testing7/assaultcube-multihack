﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Diagnostics;
using System.ComponentModel;
using System.IO;
using System.Drawing;

namespace BasicInjector
{
    public partial class MainForm
    {
        private ProcessPickerForm m_ProcessPicker;
        private ProcessPickerForm.ProcessInfoItem SelectedProcess;

        private List<int> bannedProcIDs = new List<int>();

        public void InitializeProcessWatcher()
        {
            m_ProcessPicker = new ProcessPickerForm();

            pictureBoxProcessIcon.Image = pictureBoxProcessIcon.InitialImage;
        }

        public void SetWatchedProcess(string processFileName)
        {
            SelectedProcess = new ProcessPickerForm.ProcessInfoItem();
            SelectedProcess.FilePath = processFileName;
            if (File.Exists(processFileName))
            {
                SelectedProcess.Icon = Icon.ExtractAssociatedIcon(processFileName);
                pictureBoxProcessIcon.Image = SelectedProcess.Icon.ToBitmap();
            }
            LostActiveProcess();
        }

        private void SetActiveProcess(Process proc)
        {
            SelectedProcess.ActiveProcess = proc;
            SelectedProcess.PID = SelectedProcess.ActiveProcess.Id;
            lblProcessDetails.Text = "PID: " + SelectedProcess.PID.ToString();
            lblProcessName.Text = SelectedProcess.ActiveProcess.ProcessName;
            lblProcessName.ForeColor = System.Drawing.Color.Black;
            try
            {
                SelectedProcess.Is64Bit = ProcessUtils.Is64BitProcess(SelectedProcess.ActiveProcess);
                lblProcessDetails.Text += "\n" + (SelectedProcess.Is64Bit ? "64-bit" : "32-bit");
                SelectedProcess.IsMonoLoaded = ProcessUtils.IsMonoLoaded(SelectedProcess.ActiveProcess);
                SelectedProcess.IsNetLoaded = ProcessUtils.IsNetLoaded(SelectedProcess.ActiveProcess);
                if (SelectedProcess.IsMonoLoaded)
                    lblProcessName.Text += " (mono)";
                else if (SelectedProcess.IsNetLoaded)
                    lblProcessName.Text += " (.NET)";
                else
                    lblProcessName.Text += " (No CLR)";
            }
            catch (Exception ex)
            {
                MessageBox.Show("Could not determine process features. Cannot inject.\n\n" + ex.Message + "\n\n" + ex.StackTrace, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                SelectedProcess = null;
                ProcessPollWorker.CancelAsync();
                lblProcessDetails.Text = "ERROR";
                lblProcessName.ForeColor = System.Drawing.Color.Red;
                btnInject.Enabled = false;
            }

            if (SelectedProcess.Name == "")
            {
                SelectedProcess.Name = SelectedProcess.ActiveProcess.ProcessName;
            }
            try
            {
                SelectedProcess.ActiveProcess.EnableRaisingEvents = true;
                SelectedProcess.ActiveProcess.SynchronizingObject = this;
                SelectedProcess.ActiveProcess.Exited += OnProcessExited;
            }
            catch (Exception ex)
            {
                MessageBox.Show("Could not subscribe to process' Exited event. Cannot auto-detect when selected process closes or becomes invalid.\n\n" + ex.Message + "\n\n" + ex.StackTrace, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            btnInject.Enabled = true;
        }

        private void LostActiveProcess()
        {
            btnInject.Enabled = false;
            lblProcessName.Text = "";
            lblProcessDetails.Text = "Waiting...";
            bannedProcIDs.Clear();
            // check if background worker is busy. if not, give it its task
            // if it is busy, welllll.... why would it be busy?
            if (ProcessPollWorker.IsBusy)
            {
                ProcessPollWorker.CancelAsync();
                // first, we assume the user isn't interested in other existing processes
                // so we ban existing process IDs from the polling logic
                // this ensures that we poll only for new processes
                if (SelectedProcess.ActiveProcess != null)
                {
                    Process[] existingProcesses = Process.GetProcessesByName(SelectedProcess.ActiveProcess.ProcessName);
                    foreach (Process p in existingProcesses)
                        if (!p.HasExited)
                            bannedProcIDs.Add(p.Id);
                }
                while (ProcessPollWorker.IsBusy)
                {
                    Application.DoEvents();
                }
            }
            // poll continually for new processes matching the old one's filepath
            ProcessPollWorker.RunWorkerAsync(SelectedProcess.FilePath);
        }

        private void OnProcessExited(object sender, EventArgs e)
        {
            LostActiveProcess();
        }

        private void btnSelectProcess_Click(object sender, EventArgs e)
        {
            // load process picker window
            // load process icon, process details label, and path textfield
            // upon process selection, the ProfilesTreeView's root nodes are iterated to see if a node for the selected process exists already. if not, a new node is added and a new default profile is created for that associated process. it's then set as the ActiveProfile (and SelectedNode)
            if (m_ProcessPicker.ShowDialog(this) == DialogResult.OK)
            {
                //MessageBox.Show("User selected process: " + (m_ProcessPicker.SelectedProcess.PID == -1 ? m_ProcessPicker.SelectedProcess.FilePath : m_ProcessPicker.SelectedProcess.PID.ToString()));
                SelectedProcess = m_ProcessPicker.SelectedProcess;
                if (SelectedProcess.Icon != null)
                    pictureBoxProcessIcon.Image = SelectedProcess.Icon.ToBitmap();
                txtboxPathToEXE.Text = SelectedProcess.FilePath;

                if (SelectedProcess.ActiveProcess == null)
                {
                    LostActiveProcess();
                }
                else
                {
                    SetActiveProcess(SelectedProcess.ActiveProcess);
                }
            }
        }

        private void ProcessPollWorker_DoWork(object sender, DoWorkEventArgs e)
        {
            // TO-DO:
            // better error handling
            // timeout functionality
            string processFilePath = e.Argument as string;
            Process[] allProcesses;
            while (true)
            {
                if (ProcessPollWorker.CancellationPending)
                {
                    e.Cancel = true;
                    break;
                }
                allProcesses = Process.GetProcesses();
                foreach (Process proc in allProcesses)
                {
                    try
                    {
                        if (proc.HasExited) continue;
                        if (bannedProcIDs.Contains(proc.Id)) continue;

                        ProcessModule procMainModule = proc.MainModule;
                        if (procMainModule.FileName == processFilePath)
                        {
                            e.Result = proc;
                            return;
                        }
                    }
                    // NOTE:
                    // this exception is thrown when the process is a protected process.
                    // harmless in our case
                    catch (Win32Exception w32e) { }
                    // TO-DO:
                    // error handling
                    catch (Exception ex) { }
                }
                System.Threading.Thread.Sleep(500);
            }
        }

        private void ProcessPollWorker_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            if ((e.Error != null) || (e.Cancelled))
            {
                // TO-DO:
                // handle this
                return;
            }
            if (e.Result != null)
            {
                SetActiveProcess(e.Result as Process);
            }
        }

    }
}
