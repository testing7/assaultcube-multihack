﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Diagnostics;
using System.IO;

namespace BasicInjector
{
    public partial class ProcessPickerForm : Form
    {
        public class ProcessInfoItem
        {
            public Process ActiveProcess = null;
            public int PID = -1;
            public string Name = "";
            public string FilePath = "";
            public Icon Icon = null;

            public bool Is64Bit = false;
            public bool IsMonoLoaded = false;
            public bool IsNetLoaded = false;
        }


        public ProcessInfoItem SelectedProcess;

        Process[] allProcesses;

        public ProcessPickerForm()
        {
            InitializeComponent();
        }

        private void ProcessPickerForm_Load(object sender, EventArgs e)
        {
            //MessageBox.Show("ProcessPicker loaded!");
            // TO-DO:
            // iterate *all* processes, even those with higher privileges
            // wishlist: even those whose handle is controlled by a popular (rootkit) anticheat
            openFileDialog1.InitialDirectory = Directory.GetCurrentDirectory();
            //DialogResult = DialogResult.Cancel;
            allProcesses = Process.GetProcesses();
            ProcessListView.Items.Clear();
            IconImageList.Images.Clear();
            backgroundWorker1.RunWorkerAsync();
        }

        private void ProcessPickerForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            backgroundWorker1.CancelAsync();
            // we need to null out every Process instance so we don't accumulate a ton of handles
            // so we'll block here and wait for it to finish any work
            long cycleWaster = 0;
            while (backgroundWorker1.IsBusy)
            {
                Application.DoEvents();
                cycleWaster += (long)((((float)1 / 5) / (float)0.1) / 2);
            }
            allProcesses = null;
            ProcessListView.Items.Clear();
            IconImageList.Images.Clear();
        }

        private bool GetPIDFromSelectedItem()
        {
            if (ProcessListView.SelectedItems.Count != 1)
                return false;
            SelectedProcess = (ProcessInfoItem)ProcessListView.SelectedItems[0].Tag;
            DialogResult = DialogResult.OK;
            return true;
        }

        private void ProcessListView_ItemActivate(object sender, EventArgs e)
        {
            if (GetPIDFromSelectedItem())
                Close();
        }

        private void btnSelect_Click(object sender, EventArgs e)
        {
            if (GetPIDFromSelectedItem())
                Close();
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            DialogResult = DialogResult.Cancel;
            Close();
        }

        private void btnFileSelect_Click(object sender, EventArgs e)
        {
            if (openFileDialog1.ShowDialog() == DialogResult.OK)
            {
                if (openFileDialog1.FileName != "")
                {
                    SelectedProcess = new ProcessInfoItem() { FilePath = openFileDialog1.FileName };
                    SelectedProcess.Icon = Icon.ExtractAssociatedIcon(openFileDialog1.FileName);
                    DialogResult = DialogResult.OK;
                    Close();
                }
            }
        }

        private void backgroundWorker1_DoWork(object sender, DoWorkEventArgs e)
        {
            int totalProcesses = allProcesses.Length;
            int processesProcessed = 0;
            ProcessInfoItem newProcItem = null;
            foreach (Process proc in allProcesses)
            {
                if (backgroundWorker1.CancellationPending)
                {
                    e.Cancel = true;
                    return;
                }
                try
                {
                    if (proc.HasExited) continue;
                    ProcessModule mainModule = proc.MainModule;
                    newProcItem = new ProcessInfoItem();
                    string imageKey = "";
                    if (File.Exists(mainModule.FileName))
                    {
                        newProcItem.Icon = Icon.ExtractAssociatedIcon(mainModule.FileName);
                        imageKey = proc.Id.ToString();
                        //IconImageList.Images.Add(proc.ProcessName, procIcon);
                    }
                    newProcItem.PID = proc.Id;
                    newProcItem.Name = proc.ProcessName;
                    newProcItem.FilePath = mainModule.FileName;
                    newProcItem.ActiveProcess = proc;
                    //ProcessListView.Items.Add(new ListViewItem(proc.Id + "-" + proc.ProcessName, proc.ProcessName) { Tag = proc.Id });
                }
                // privileged processes will throw an exception when trying to access MainModule
                // handling this is on the TO-DO / wishlist above. i just don't want to set up all the p/invokes right now.
                catch (Win32Exception w32e)
                {
                    newProcItem = null;
                }
                catch (Exception ex)
                {
                    newProcItem = null;
                }
                finally
                {
                    backgroundWorker1.ReportProgress((int)((++processesProcessed / (float)totalProcesses) * 100f), newProcItem);
                }
            }
        }

        private void backgroundWorker1_ProgressChanged(object sender, ProgressChangedEventArgs e)
        {
            progressBar1.Value = e.ProgressPercentage;
            ProcessInfoItem procItem = (ProcessInfoItem)e.UserState;
            if (procItem == null) return;

            if (procItem.Icon != null)
                IconImageList.Images.Add(procItem.PID.ToString(), procItem.Icon);
            ProcessListView.Items.Add(new ListViewItem(procItem.PID + "-" + procItem.Name, procItem.PID.ToString()) { Tag = procItem });
        }

        private void backgroundWorker1_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            progressBar1.Visible = false;
            this.Size = new Size(321, 365);
        }
    }
}
