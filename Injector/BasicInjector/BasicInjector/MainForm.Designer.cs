﻿namespace BasicInjector
{
    partial class MainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.pictureBoxProcessIcon = new System.Windows.Forms.PictureBox();
            this.txtboxPathToEXE = new System.Windows.Forms.TextBox();
            this.lblProcessDetails = new System.Windows.Forms.Label();
            this.lblProcessName = new System.Windows.Forms.Label();
            this.btnSelectProcess = new System.Windows.Forms.Button();
            this.btnInject = new System.Windows.Forms.Button();
            this.txtboxPathToDLL = new System.Windows.Forms.TextBox();
            this.btnSelectDLL = new System.Windows.Forms.Button();
            this.openFileDialog1 = new System.Windows.Forms.OpenFileDialog();
            this.ProcessPollWorker = new System.ComponentModel.BackgroundWorker();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxProcessIcon)).BeginInit();
            this.SuspendLayout();
            // 
            // pictureBoxProcessIcon
            // 
            this.pictureBoxProcessIcon.Location = new System.Drawing.Point(12, 12);
            this.pictureBoxProcessIcon.Name = "pictureBoxProcessIcon";
            this.pictureBoxProcessIcon.Size = new System.Drawing.Size(32, 32);
            this.pictureBoxProcessIcon.TabIndex = 0;
            this.pictureBoxProcessIcon.TabStop = false;
            // 
            // txtboxPathToEXE
            // 
            this.txtboxPathToEXE.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtboxPathToEXE.Enabled = false;
            this.txtboxPathToEXE.Location = new System.Drawing.Point(12, 50);
            this.txtboxPathToEXE.Name = "txtboxPathToEXE";
            this.txtboxPathToEXE.ReadOnly = true;
            this.txtboxPathToEXE.Size = new System.Drawing.Size(253, 20);
            this.txtboxPathToEXE.TabIndex = 1;
            // 
            // lblProcessDetails
            // 
            this.lblProcessDetails.AutoSize = true;
            this.lblProcessDetails.Location = new System.Drawing.Point(50, 12);
            this.lblProcessDetails.Name = "lblProcessDetails";
            this.lblProcessDetails.Size = new System.Drawing.Size(40, 13);
            this.lblProcessDetails.TabIndex = 2;
            this.lblProcessDetails.Text = "PID: -1";
            // 
            // lblProcessName
            // 
            this.lblProcessName.AutoSize = true;
            this.lblProcessName.Location = new System.Drawing.Point(50, 34);
            this.lblProcessName.Name = "lblProcessName";
            this.lblProcessName.Size = new System.Drawing.Size(76, 13);
            this.lblProcessName.TabIndex = 3;
            this.lblProcessName.Text = "Process Name";
            // 
            // btnSelectProcess
            // 
            this.btnSelectProcess.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnSelectProcess.Location = new System.Drawing.Point(271, 34);
            this.btnSelectProcess.Name = "btnSelectProcess";
            this.btnSelectProcess.Size = new System.Drawing.Size(67, 36);
            this.btnSelectProcess.TabIndex = 4;
            this.btnSelectProcess.Text = "Select Process";
            this.btnSelectProcess.UseVisualStyleBackColor = true;
            this.btnSelectProcess.Click += new System.EventHandler(this.btnSelectProcess_Click);
            // 
            // btnInject
            // 
            this.btnInject.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.btnInject.Enabled = false;
            this.btnInject.Location = new System.Drawing.Point(12, 118);
            this.btnInject.Name = "btnInject";
            this.btnInject.Size = new System.Drawing.Size(326, 35);
            this.btnInject.TabIndex = 5;
            this.btnInject.Text = "Inject";
            this.btnInject.UseVisualStyleBackColor = true;
            this.btnInject.Click += new System.EventHandler(this.btnInject_Click);
            // 
            // txtboxPathToDLL
            // 
            this.txtboxPathToDLL.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtboxPathToDLL.Enabled = false;
            this.txtboxPathToDLL.Location = new System.Drawing.Point(12, 92);
            this.txtboxPathToDLL.Name = "txtboxPathToDLL";
            this.txtboxPathToDLL.ReadOnly = true;
            this.txtboxPathToDLL.Size = new System.Drawing.Size(253, 20);
            this.txtboxPathToDLL.TabIndex = 6;
            // 
            // btnSelectDLL
            // 
            this.btnSelectDLL.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnSelectDLL.Location = new System.Drawing.Point(271, 76);
            this.btnSelectDLL.Name = "btnSelectDLL";
            this.btnSelectDLL.Size = new System.Drawing.Size(67, 36);
            this.btnSelectDLL.TabIndex = 7;
            this.btnSelectDLL.Text = "Select DLL";
            this.btnSelectDLL.UseVisualStyleBackColor = true;
            this.btnSelectDLL.Click += new System.EventHandler(this.btnSelectDLL_Click);
            // 
            // openFileDialog1
            // 
            this.openFileDialog1.DefaultExt = "dll";
            this.openFileDialog1.Filter = "DLL Files|*.dll";
            this.openFileDialog1.RestoreDirectory = true;
            this.openFileDialog1.Title = "Select DLL to inject...";
            // 
            // ProcessPollWorker
            // 
            this.ProcessPollWorker.WorkerSupportsCancellation = true;
            this.ProcessPollWorker.DoWork += new System.ComponentModel.DoWorkEventHandler(this.ProcessPollWorker_DoWork);
            this.ProcessPollWorker.RunWorkerCompleted += new System.ComponentModel.RunWorkerCompletedEventHandler(this.ProcessPollWorker_RunWorkerCompleted);
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(350, 165);
            this.Controls.Add(this.btnSelectDLL);
            this.Controls.Add(this.txtboxPathToDLL);
            this.Controls.Add(this.btnInject);
            this.Controls.Add(this.btnSelectProcess);
            this.Controls.Add(this.lblProcessName);
            this.Controls.Add(this.lblProcessDetails);
            this.Controls.Add(this.txtboxPathToEXE);
            this.Controls.Add(this.pictureBoxProcessIcon);
            this.Name = "MainForm";
            this.Text = "BasicInjector";
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxProcessIcon)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.PictureBox pictureBoxProcessIcon;
        private System.Windows.Forms.TextBox txtboxPathToEXE;
        private System.Windows.Forms.Label lblProcessDetails;
        private System.Windows.Forms.Label lblProcessName;
        private System.Windows.Forms.Button btnSelectProcess;
        private System.Windows.Forms.Button btnInject;
        private System.Windows.Forms.TextBox txtboxPathToDLL;
        private System.Windows.Forms.Button btnSelectDLL;
        private System.Windows.Forms.OpenFileDialog openFileDialog1;
        private System.ComponentModel.BackgroundWorker ProcessPollWorker;
    }
}

