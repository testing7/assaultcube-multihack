﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace BasicInjector
{
    public partial class MainForm : Form
    {
        public MainForm()
        {
            InitializeComponent();

            InitializeProcessWatcher();
        }

        private void btnSelectDLL_Click(object sender, EventArgs e)
        {
            if (openFileDialog1.ShowDialog() == DialogResult.OK)
            {
                if (openFileDialog1.FileName != "")
                {
                    txtboxPathToDLL.Text = openFileDialog1.FileName;
                }
            }
        }

        private void btnInject_Click(object sender, EventArgs e)
        {
            if (SelectedProcess.ActiveProcess == null)
                return;
            SelectedProcess.ActiveProcess.Refresh();
            if (string.IsNullOrEmpty(SelectedProcess.FilePath))
                return;
            if (SelectedProcess.ActiveProcess.HasExited)
            {
                if (ProcessPollWorker.IsBusy)
                    return;
            }
            try
            {
                // TO-DO:
                // verify that the target process is the same architecture as DLL
                Injector.Inject(SelectedProcess.ActiveProcess, txtboxPathToDLL.Text);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Basic Injector", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
    }
}
