﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.InteropServices;
using System.Diagnostics;
using BasicInjector.Native;
using System.Windows.Forms;

namespace BasicInjector
{
    // i think i found this class somewhere online, but i can't remember where
    // so, thanks random internet person
    public static class Injector
    {
        /// <summary>
        /// 1. Calls OpenProcess to get a handle (w/ sufficient privileges) to the target process
        /// 2. Gets the address of LoadLibraryA (see TO-DO note on how to improve this)
        /// 3. Calls VirtualAllocEx to allocate room for our PathToDLL string
        /// 4. Calls WriteProcessMemory to write our PathToDLL string to the newly allocated memory in the target process
        /// 5. Calls CreateRemoteThread on the address of LoadLibraryA w/ the ptr to the allocated memory that now holds our PathToDLL string as the thread parameter
        /// 6. Closes all opened handles. Proc handle and thread handle.
        /// </summary>
        /// <param name="TargetProcess"></param>
        /// <param name="PathToDLL"></param>
        /// <returns></returns>
        public static bool Inject(Process TargetProcess, string PathToDLL)
        {
            bool returnValue = true;
            System.Windows.Forms.MessageBox.Show("Injecting '" + PathToDLL + "' into " + TargetProcess.ProcessName);
            IntPtr procHandle = Imports.OpenProcess((Imports.PROCESS_CREATE_THREAD | Imports.PROCESS_QUERY_INFORMATION | Imports.PROCESS_VM_OPERATION | Imports.PROCESS_VM_WRITE | Imports.PROCESS_VM_READ), 1, (uint)TargetProcess.Id);
            if (procHandle == (IntPtr)0)
            {
                throw new Exception("Could not get handle to target process: " + Marshal.GetLastWin32Error().ToString());
            }

            IntPtr hKernel32 = Imports.GetModuleHandle("kernel32.dll");
            if (hKernel32 == (IntPtr)0)
            {
                throw new Exception("Could not get handle to kernel32.dll (this is impossible): " + Marshal.GetLastWin32Error().ToString());
            }

            // for 64bit this should ALWAYS be 7ffcf967eb60
            // for 32bit this should ALWAYS be 749C2990
            // TO-DO:
            // need to make sure this is the LoadLibraryA address that will match up with the target process.
            // the problem is that if this process is 64bit and the target is 32bit, it'll try using the 64bit address (which is obviously invalid in a 32bit process).
            // one solution is to copy over the NT header and search for the function & address in the IAT (that way it'll always be accurate)
            IntPtr loadLibraryAddr = Imports.GetProcAddress(hKernel32, "LoadLibraryA");
            if (loadLibraryAddr == (IntPtr)0)
            {
                throw new Exception("Could not find LoadLibrary in process (this is also impossible): " + Marshal.GetLastWin32Error().ToString());
            }

            IntPtr allocMemAddress = Imports.VirtualAllocEx(procHandle, (IntPtr)null, (IntPtr)PathToDLL.Length + 1, (Imports.MEM_COMMIT | Imports.MEM_RESERVE), Imports.PAGE_EXECUTE_READWRITE);
            if (allocMemAddress == (IntPtr)0)
            {
                throw new Exception("Could not allocate memory in target process: " + Marshal.GetLastWin32Error().ToString());
            }

            byte[] bytes = Encoding.ASCII.GetBytes(PathToDLL);
            IntPtr bytesWritten;
            int written = Imports.WriteProcessMemory(procHandle, allocMemAddress, bytes, (uint)bytes.Length, out bytesWritten);
            if ((int)bytesWritten != bytes.Length)
            {
                throw new Exception("Could not write to target process: " + Marshal.GetLastWin32Error().ToString());
            }
            //MessageBox.Show("Alloc Addr: " + allocMemAddress.ToString("X2") + "\nLoadLibrary Addr: " + loadLibraryAddr.ToString("X2"));
            IntPtr ipThread = Imports.CreateRemoteThread(procHandle, (IntPtr)null, (IntPtr)0, loadLibraryAddr, allocMemAddress, 0, (IntPtr)null);
            if (ipThread == (IntPtr)0)
            {
                throw new Exception("Could not create remote thread in target process: " + Marshal.GetLastWin32Error().ToString());
            }

            // oh god i'm dumb
            // this will wait on the thread that just calls LoadLibrary
            // that thread will exit immediately after LoadLibrary returns (regardless of what the return value is)
            // i think the thread's exit code is actually set to the result of LoadLibrary... strange.
            uint waitValue = Imports.WaitForSingleObject(ipThread, Imports.INFINITE);
            if (waitValue == Imports.WAIT_OBJECT_0)
            {
                uint exitCode = 50;
                if (!Imports.GetExitCodeThread(ipThread, out exitCode))
                {
                    throw new Exception("Could not get exit code from remote thread: " + Marshal.GetLastWin32Error().ToString());
                }
                if (exitCode == 0)
                {
                    throw new Exception("Could not load bootstrapper into target process. No further details available.");
                }
            }

            // TO-DO:
            // regardless, what i want is to get the return value from the library when it returns from DLLMain.
            // that is much harder, i think.

            Imports.CloseHandle(ipThread);
            Imports.CloseHandle(procHandle);

            return returnValue;
        }
    }
}
