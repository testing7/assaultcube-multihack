#include "main.h"

LPCWSTR WindowName = L"Assault Cube Multihack - External";
LPCWSTR GameExeName = L"ac_client.exe";
HINSTANCE myInstance = NULL;
ESPWindow_D3D11* espWindow = NULL;

HWND GameWindow = NULL;
HANDLE GameHandle = NULL;

bool shouldDoStatCheats = false;
bool hasSetGunInfoStats = false;
playerent_wrapper* aimbotTargetPlayer = NULL;

void GetGameWindow()
{
    while (GameHandle == NULL)
    {
        HANDLE procSnapshot = CreateToolhelp32Snapshot(TH32CS_SNAPPROCESS, 0);
        if (procSnapshot == INVALID_HANDLE_VALUE)
        {
            // TO-DO:
            // error reporting
            continue;
        }
        PROCESSENTRY32 pe32;
        pe32.dwSize = sizeof(PROCESSENTRY32);
        if (Process32First(procSnapshot, &pe32) == FALSE)
        {
            CloseHandle(procSnapshot);
            // TO-DO:
            // error reporting
            continue;
        }
        do
        {
            if (_wcsicmp(pe32.szExeFile, L"ac_client.exe") == 0)
            {
                // found game, get handle and exit loop
                GameHandle = OpenProcess(PROCESS_ALL_ACCESS, FALSE, pe32.th32ProcessID);
                if (GameHandle == NULL)
                {
                    break;
                    // TO-DO:
                    // error reporting
                }
            }
        } while (Process32Next(procSnapshot, &pe32));

        CloseHandle(procSnapshot);
        Sleep(100);
    }
    while (GameWindow == NULL)
    {
        GameWindow = FindWindow(NULL, L"AssaultCube");
        Sleep(100);
    }
}

void EnableDebugPrivilege()
{
    HANDLE hToken = NULL;
    TOKEN_PRIVILEGES tokenPriv;
    LUID luidDebug;
    printf("Enabling debug privilege...\n");
    if (OpenProcessToken(GetCurrentProcess(), TOKEN_ADJUST_PRIVILEGES, &hToken) != FALSE)
    {
        if (LookupPrivilegeValue(NULL, SE_DEBUG_NAME, &luidDebug) != FALSE)
        {
            tokenPriv.PrivilegeCount = 1;
            tokenPriv.Privileges[0].Luid = luidDebug;
            tokenPriv.Privileges[0].Attributes = SE_PRIVILEGE_ENABLED;
            if (AdjustTokenPrivileges(hToken, FALSE, &tokenPriv, 0, NULL, NULL) != FALSE)
            {
                // Always successful, even in the cases which lead to OpenProcess failure
                printf("Successfully enabled debug privilege\n");
            }
            else
            {
                printf("Failed to enable debug privilege (GetLastError(): %d)\n", GetLastError());
            }
        }
        else
        {
            printf("Failed to LookupPrivilegeValue\n");
        }
    }
    else
    {
        printf("Failed to OpenProcessToken\n");
    }
    printf("Closing token handle... Done.\n");
    CloseHandle(hToken);
}

// credit: Rake via [https://guidedhacking.com/threads/ultimate-calcangle-thread-how-to-calculate-angle-functions.8165/#post-43092]
vec CalcAngle(vec src, vec dst)
{
    vec angle;
    float dist = src.dist(dst);

    // pitch, yaw, roll
    angle.x = asinf((dst.z - src.z) / dist) * Rad2Deg;
    angle.y = -atan2f(dst.x - src.x, dst.y - src.y) / PI * 180.0f + 180.0f;
    angle.z = 0.0f;

    return angle;
}

void DoAimbot(assaultcube_wrapper* game)
{
    if (game->localplayer->state != 0x0)
        return;
    vec myEyePos = game->localplayer->origin;
    myEyePos.z += game->localplayer->eyeheight;
    int myTeam = game->localplayer->team;
    vec myHeading = vec(game->localplayer->pitch, game->localplayer->yaw, 0.f);

    float closestToCrosshairDiff = 3.402823466e+38F;
    vec closestAngle = vec(3.402823466e+38F, 3.402823466e+38F, 0.f);
    aimbotTargetPlayer = NULL;
    for (std::vector<botent_wrapper>::size_type index = 0; index < game->bots->size(); index++)
    {
        botent_wrapper player = (botent_wrapper)game->bots->at(index);
        if ((player.state != 0x0) || (player.team == myTeam)) continue;
        vec enemyEyePos = player.origin;
        enemyEyePos.z += player.eyeheight;
        vec angle = CalcAngle(myEyePos, enemyEyePos);
        float diff = myHeading.dist(angle);
        if (diff < closestToCrosshairDiff)
        {
            closestToCrosshairDiff = diff;
            closestAngle = angle;
            // i don't like this but it'll do for now
            aimbotTargetPlayer = &game->bots[0][index];
        }
    }
    if (aimbotTargetPlayer != NULL)
    {
        // yaw, pitch, roll
        AssaultCubeMemoryReader::WriteYawAndPitch(closestAngle.y, closestAngle.x);
    }
}

void DoStatCheats(assaultcube_wrapper* game)
{
    if (game->localplayer->state != 0x0)
        return;
    AssaultCubeMemoryReader::WriteHealthAndArmor();
    AssaultCubeMemoryReader::WriteAmmo();
    if (hasSetGunInfoStats == false)
    {
        hasSetGunInfoStats = true;
        AssaultCubeMemoryReader::WriteGunInfo();
    }
}

void DoFrame()
{
    assaultcube_wrapper* game = AssaultCubeMemoryReader::Update();
    if (game == NULL)
    {
        // TO-DO:
        // start polling for process again, then open handle to new process and call AssaultCubeMemoryReader::InitializeForProcess().
        return;
    }

    CheckInput();

    // ESP-related stuff (read-only)

    // aimbot & stat-related stuff (read & write)
    if (Input::IsKeyPressed(VK_LBUTTON, true))
    {
        DoAimbot(game);
    }
    if (shouldDoStatCheats)
    {
        DoStatCheats(game);
    }
}

void CheckInput()
{
    if (Input::IsKeyPressed(VK_NUMPAD1))
    {
        shouldDoStatCheats = !shouldDoStatCheats;
    }
    // TO-DO:
    // all input handling here, except the mouse-button aimbot
}

LRESULT CALLBACK WndProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam)
{
    // if we had more than one window we'd sort and pass it to the appropriate window's WndProc
    return espWindow->WndProc(hWnd, message, wParam, lParam);
}

void Shutdown()
{
    CloseHandle(GameHandle);
    CloseHandle(GameWindow);
}

int APIENTRY wWinMain(_In_ HINSTANCE hInstance,
    _In_opt_ HINSTANCE hPrevInstance,
    _In_ LPWSTR    lpCmdLine,
    _In_ int       nCmdShow)
{
    myInstance = hInstance;
    GetGameWindow();
    // initialize memory reader
    AssaultCubeMemoryReader::InitializeForProcess(GameHandle);
    MSG msg;

    espWindow = new ESPWindow_D3D11(myInstance, L"Assault Cube Multihack - External (D3D11)", WndProc, DoFrame);
    bool result = espWindow->Initialize(nCmdShow);
    if (result)
    {
        espWindow->SetGameWindow(GameWindow);
        msg = espWindow->Run();
    }
    else
    {
        MessageBox(NULL, L"Could not initialize ESPWindow", L"Assault Cube Multihack - External (D3D11)", 0);
        // TO-DO:
        // verify that this makes sense for a return value
        msg.wParam = 0;
    }

    espWindow->Shutdown();
    delete espWindow;
    espWindow = 0;

    CloseHandle(GameHandle);
    CloseHandle(GameWindow);

    return (int)msg.wParam;
}