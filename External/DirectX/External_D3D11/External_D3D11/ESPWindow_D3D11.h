#pragma once

#include <Windows.h>

#include "D3DClass.h"
#include "ShaderManagerClass.h"



#include "TextClass.h"
#include "LineBatcher.h"

#include "../../../Common/EngineStructs.h"

// TO-DO:
// finish initializing d3d11 (shader manager (and color shader), camera, text manager, line manager)
// set up Render functions, and BeginScene / EndScene
// might be a good idea to put all the d3d11 related stuff in its own class, and have this ESPWindow act more like the GraphicsClass from rastertek
class ESPWindow_D3D11
{
public:
	// registers the window class
	ESPWindow_D3D11(HINSTANCE instance, LPCWSTR name, LRESULT(__stdcall* globalWndProc)(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam), void(*dOnFrame)(void));

private:
	HINSTANCE Instance = NULL;
	LPCWSTR WindowName;
	HWND HWindow = NULL;
	HWND GameWindow = NULL;
	LRESULT(__stdcall* GlobalWndProc)(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam);
	void(*OnFrame)(void);
	// d3d11 stuff
	D3DClass* m_D3D;
	ShaderManagerClass* m_ShaderManager;
	// maybe don't need a camera, can probably implement most of its logic in this class
	// yeah, we'd only be using the CameraClass to calculate the viewmatrix, but we don't even have to do that because we have the game's own viewmatrix already.
	//CameraClass* m_Camera;
	TextClass* m_TextManager;
	LineBatcher* m_LineBatcher;
	// end d3d11 stuff

public:
	const float SCREEN_DEPTH = 1000.0f;
	const float SCREEN_NEAR = 0.1f;

	RECT GameWindowRect;
	RECT GameClientRect;
	float GameWidth, GameHeight;
	float ViewportWidth, ViewportHeight, HalfVPWidth, HalfVPHeight;
	float OurWidth, OurHeight;

private:
	void ResizeWindow();
	bool SetupD3D11(int screenWidth, int screenHeight);
	void ShutdownD3D11();
	ATOM DoRegisterClass(HINSTANCE hInstance);
	bool Render();

public:
	// creates the window
	bool Initialize(HWND gameWindow);
	void Shutdown();
	// windows message pump
	MSG Run();
	// windows message handler
	LRESULT CALLBACK WndProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam);

	void SetGameWindow(HWND gameWindow);
};

