#include "ESPWindow_D3D11.h"


ESPWindow_D3D11::ESPWindow_D3D11(HINSTANCE instance, LPCWSTR name, LRESULT(__stdcall* globalWndProc)(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam), void(*dOnFrame)(void))
{
    Instance = instance;
    WindowName = name;
    GlobalWndProc = globalWndProc;
    OnFrame = dOnFrame;

    DoRegisterClass(Instance);

    // initialize all d3d11 resources to 0
    m_D3D = 0;
    m_ShaderManager = 0;
	m_TextManager = 0;
	m_LineBatcher = 0;
}

ATOM ESPWindow_D3D11::DoRegisterClass(HINSTANCE hInstance)
{
    WNDCLASSEXW wcex;
    wcex.cbSize = sizeof(WNDCLASSEX);

    //wcex.style = CS_HREDRAW | CS_VREDRAW;
    wcex.style = 0;
    wcex.lpfnWndProc = GlobalWndProc;
    wcex.cbClsExtra = 0;
    wcex.cbWndExtra = 0;
    wcex.hInstance = hInstance;
    wcex.hIcon = NULL;
    wcex.hCursor = NULL;
    //wcex.hbrBackground = (HBRUSH)(COLOR_WINDOW + 1);
    wcex.hbrBackground = CreateSolidBrush(RGB(0, 0, 0));
    wcex.lpszMenuName = NULL;
    wcex.lpszClassName = WindowName;
    wcex.hIconSm = NULL;

    return RegisterClassExW(&wcex);
}

//resize and reposition window
void ESPWindow_D3D11::ResizeWindow()
{
	if (GameWindow == NULL) return;

	//get physical window bounds
	GetWindowRect(GameWindow, &GameWindowRect);
	//get logical client bounds
	GetClientRect(GameWindow, &GameClientRect);
	//width and height of client rect
	GameWidth = GameClientRect.right - GameClientRect.left;
	GameHeight = GameClientRect.bottom - GameClientRect.top;

	float WindowWidth = GameWindowRect.right - GameWindowRect.left;
	float WindowHeight = GameWindowRect.bottom - GameWindowRect.top;
	// the frame of the window (if window is decorated as such. no way of getting the actual value, so we approximate) - should only be like 2 pixels.
	float frameWidth = (WindowWidth - GameWidth) * 0.5f;
	// again, more window decoration. i think a tiny frame is added between the titlebar and the game rect. not entirely sure. close enough either way.
	float titlebarHeight = (WindowHeight - GameHeight);
	ViewportWidth = WindowWidth - (frameWidth * 2);
	ViewportHeight = WindowHeight - titlebarHeight - frameWidth;
	HalfVPWidth = ViewportWidth * 0.5f;
	HalfVPHeight = ViewportHeight * 0.5f;
	OurWidth = WindowWidth - (frameWidth * 2);
	OurHeight = WindowHeight - titlebarHeight - frameWidth;

	if (HWindow != NULL)
	{
		SetWindowPos(HWindow, 0, GameWindowRect.left + frameWidth, GameWindowRect.top + titlebarHeight, OurWidth, OurHeight, 0);
		// TO-DO:
		// backbuffer, depth buffer, viewport, projection matrix, and ortho matrix all need to be reinitialized w/ new size
		// should probably think about how i can separate the d3d dimensions from the window dimensions...
		// or i can just not support resizing the window :) :) :)
	}
}

bool ESPWindow_D3D11::Initialize(HWND gameWindow)
{
	// TO-DO:
	// make sure any d3d stuff in SetGameWindow doesn't run the first time it's called (because right now the d3d state isn't valid)
	SetGameWindow(gameWindow);
	return SetupD3D11(OurWidth, OurHeight);
}

void ESPWindow_D3D11::Shutdown()
{

	ShutdownD3D11();

	// Remove the window.
	DestroyWindow(HWindow);
	HWindow = NULL;

	UnregisterClass(WindowName, Instance);
}

bool ESPWindow_D3D11::SetupD3D11(int screenWidth, int screenHeight)
{
	bool result;

	D3DXMATRIX baseViewMatrix;

	// Create the Direct3D object.
	m_D3D = new D3DClass;
	if (!m_D3D)
	{
		return false;
	}

	// Initialize the Direct3D object.
	result = m_D3D->Initialize(screenWidth, screenHeight, HWindow, SCREEN_DEPTH, SCREEN_NEAR);
	if (!result)
	{
		MessageBox(HWindow, L"Could not initialize Direct3D", L"Error", MB_OK);
		return false;
	}

	// Create the shader manager object.
	m_ShaderManager = new ShaderManagerClass;
	if (!m_ShaderManager)
	{
		return false;
	}

	// Initialize the shader manager object.
	result = m_ShaderManager->Initialize(m_D3D->GetDevice(), HWindow);
	if (!result)
	{
		MessageBox(HWindow, L"Could not initialize the shader manager object.", L"Error", MB_OK);
		return false;
	}

    // TO-DO:
    // set baseViewMatrix before continuing!

	// Create the text object.
	m_TextManager = new TextClass;
	if (!m_TextManager)
	{
		return false;
	}

	// Initialize the text object.
	result = m_TextManager->Initialize(m_D3D->GetDevice(), m_D3D->GetDeviceContext(), HWindow, screenWidth, screenHeight, baseViewMatrix);
	if (!result)
	{
		MessageBox(HWindow, L"Could not initialize the text manager object.", L"Error", MB_OK);
		return false;
	}


	// Create the line manager object.
	m_LineBatcher = new LineBatcher;
	if (!m_LineBatcher)
	{
		return false;
	}

	// Initialize the shader manager object.
	result = m_LineBatcher->Initialize(m_D3D->GetDevice());
	if (!result)
	{
		MessageBox(HWindow, L"Could not initialize the line batcher object.", L"Error", MB_OK);
		return false;
	}

	return true;
}

void ESPWindow_D3D11::ShutdownD3D11()
{
	// Release the text object.
	if (m_TextManager)
	{
		m_TextManager->Shutdown();
		delete m_TextManager;
		m_TextManager = 0;
	}

	// Release the line manager object.
	if (m_LineBatcher)
	{
		m_LineBatcher->Shutdown();
		delete m_LineBatcher;
		m_LineBatcher = 0;
	}

	// Release the shader manager object.
	if (m_ShaderManager)
	{
		m_ShaderManager->Shutdown();
		delete m_ShaderManager;
		m_ShaderManager = 0;
	}

	// Release the D3D object.
	if (m_D3D)
	{
		m_D3D->Shutdown();
		delete m_D3D;
		m_D3D = 0;
	}

	return;
}

bool ESPWindow_D3D11::Render()
{
	D3DXMATRIX viewMatrix, projectionMatrix, worldMatrix, orthoMatrix;
	bool result;


	// Clear the buffers to begin the scene.
	m_D3D->BeginScene(0.0f, 0.0f, 0.0f, 1.0f);

	// TO-DO:
	// fill in viewMatrix before continuing!

	m_D3D->GetWorldMatrix(worldMatrix);
	m_D3D->GetProjectionMatrix(projectionMatrix);
	// We now also get the ortho matrix from the D3DClass for 2D rendering. We will pass this in instead of the projection matrix.
	m_D3D->GetOrthoMatrix(orthoMatrix);

	// BEGIN LINE RENDERING (worldspace)
	m_LineBatcher->ClearLines();
	// show green line along the forward axis
	m_LineBatcher->AddLine(D3DXVECTOR3(0.f, 0.f, -5.f), D3DXVECTOR3(0.f, 0.f, 5.f), D3DXVECTOR4(0.f, 1.f, 0.f, 1.f));
	// show red line along the left-right axis
	m_LineBatcher->AddLine(D3DXVECTOR3(-5.f, 0.f, 0.f), D3DXVECTOR3(5.f, 0.f, 0.f), D3DXVECTOR4(1.f, 0.f, 0.f, 1.f));
	// show blue line along the up axis
	m_LineBatcher->AddLine(D3DXVECTOR3(0.f, -5.f, 0.f), D3DXVECTOR3(0.f, 5.f, 0.f), D3DXVECTOR4(0.f, 0.f, 1.f, 1.f));
	m_LineBatcher->Render(m_D3D->GetDeviceContext());
	if (m_LineBatcher->GetIndexCount() > 0)
	{
		result = m_ShaderManager->RenderColorShader(m_D3D->GetDeviceContext(), m_LineBatcher->GetIndexCount(), worldMatrix, viewMatrix, projectionMatrix);
		if (!result)
		{
			return false;
		}
	}
	// END LINE RENDERING

	// BEGIN 2D RENDERING
	m_D3D->GetWorldMatrix(worldMatrix);
	// Turn off the Z buffer to begin all 2D rendering.
	m_D3D->TurnZBufferOff();

	// BEGIN TEXT RENDERING
	// Turn on the alpha blending before rendering the text.
	//m_D3D->TurnOnAlphaBlending();
	// Render the text strings.
	result = m_TextManager->Render(m_D3D->GetDeviceContext(), worldMatrix, orthoMatrix);
	if (!result)
	{
		return false;
	}
	// Turn off alpha blending after rendering the text.
	//m_D3D->TurnOffAlphaBlending();
	// END TEXT RENDERING

	// Turn the Z buffer back on now that all 2D rendering has completed.
	m_D3D->TurnZBufferOn();
	// END 2D RENDERING


	// Present the rendered scene to the screen.
	m_D3D->EndScene();

	return true;
}

LRESULT CALLBACK ESPWindow_D3D11::WndProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam)
{
    switch (message)
    {
    case WM_CLOSE:
    {
		PostQuitMessage(0);
        break;
    }
    case WM_DESTROY:
	{
		// NOTE:
		// we moved shutdown code to WinMain()

		PostQuitMessage(0);
		break;
	}
    default:
        return DefWindowProc(hWnd, message, wParam, lParam);
    }
    return 0;
}

MSG ESPWindow_D3D11::Run()
{
    // main loop, using PeekMessage and processing entire queue, THEN invalidating client rect to generate a redraw msg.
    // can add timing to constrain the redraws to a certain FPS, too
    MSG msg;
    while (true)
    {
        // message pump, always fully clear so windows doesn't think we're frozen
        while (PeekMessage(&msg, NULL, 0, 0, PM_REMOVE))
        {
            TranslateMessage(&msg);
            DispatchMessage(&msg);
            if (msg.message == WM_QUIT)
                break;
        }
        // main rendering loop:
		// TO-DO: 
		// check if Render failed, exit out if so
		Render();
        Sleep(5);
    }
    // we are shutting down now, so return to WinMain (and then return from there)
    return msg;
}

void ESPWindow_D3D11::SetGameWindow(HWND gameWindow)
{
    GameWindow = gameWindow;
    ResizeWindow();
}