#pragma once

///////////////////////
// MY CLASS INCLUDES //
///////////////////////
#include "D3DClass.h"

#include "ColorShaderClass.h"

class ShaderManagerClass
{
public:
	ShaderManagerClass();
	ShaderManagerClass(const ShaderManagerClass&);
	~ShaderManagerClass();

	bool Initialize(ID3D11Device*, HWND);
	void Shutdown();

	bool RenderColorShader(ID3D11DeviceContext*, int, D3DXMATRIX, D3DXMATRIX, D3DXMATRIX);

private:
	ColorShaderClass* m_ColorShader;
};