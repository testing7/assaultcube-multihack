#pragma once

#include <d3d11.h>
#include <d3dx10math.h>

const int MAX_LINES = 32;

class LineBatcher
{
private:
	struct VertexType
	{
		// used for the data in the vertex buffer
		D3DXVECTOR3 position;
		D3DXVECTOR4 color;
	};
public:
	LineBatcher();
	LineBatcher(const LineBatcher&);
	~LineBatcher();

	bool Initialize(ID3D11Device*);
	void Shutdown();
	void Render(ID3D11DeviceContext*);
	int GetIndexCount();

	bool AddLine(D3DXVECTOR3, D3DXVECTOR3, D3DXVECTOR4);
	// should be called at the start of every frame (clear vertex and index buffers, set m_NumLines to 0)
	void ClearLines();

private:
	bool InitializeBuffers(ID3D11Device*);
	void ShutdownBuffers();
	bool PrepareBuffers(ID3D11DeviceContext*);
	void RenderBuffers(ID3D11DeviceContext*);

private:
	ID3D11Buffer* m_vertexBuffer, * m_indexBuffer;

	VertexType* m_Vertices;
	int m_NumLines;
};