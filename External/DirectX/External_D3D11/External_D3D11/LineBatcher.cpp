#include "LineManager.h"

LineBatcher::LineBatcher()
{
	m_vertexBuffer = 0;
	m_indexBuffer = 0;

	// not a ptr but initializing it to 0 to prevent a bug when accidentally calling Render before ClearLines.
	m_NumLines = 0;
}


LineBatcher::LineBatcher(const LineBatcher& other)
{
}


LineBatcher::~LineBatcher()
{
}

bool LineBatcher::Initialize(ID3D11Device* device)
{
	bool result;

	result = InitializeBuffers(device);
	if (!result)
	{
		return false;
	}
	return true;
}

bool LineBatcher::InitializeBuffers(ID3D11Device* device)
{
	unsigned long* indices;
	D3D11_BUFFER_DESC vertexBufferDesc, indexBufferDesc;
	D3D11_SUBRESOURCE_DATA indexData;
	HRESULT result;

	int index;

	// create the vertices array
	// this will exist for the lifetime of the manager and will always hold every vert for every line
	m_Vertices = new VertexType[MAX_LINES * 2];

	// Create the index array.
	indices = new unsigned long[MAX_LINES * 2];
	if (!indices)
	{
		return false;
	}

	// populate the indices array here since it never changes
	for (index = 0; index < (MAX_LINES * 2); index++)
	{
		indices[index] = index;
	}

	// Set up the description of the vertex buffer.
	// make it dynamic because we can add / remove lines per-frame
	vertexBufferDesc.Usage = D3D11_USAGE_DYNAMIC;
	// each line is 2 verts
	vertexBufferDesc.ByteWidth = sizeof(VertexType) * (MAX_LINES * 2);
	vertexBufferDesc.BindFlags = D3D11_BIND_VERTEX_BUFFER;
	vertexBufferDesc.CPUAccessFlags = D3D11_CPU_ACCESS_WRITE;
	vertexBufferDesc.MiscFlags = 0;
	vertexBufferDesc.StructureByteStride = 0;

	// Now create the vertex buffer.
	// we pass null for the second parameter because we don't have any pre-existing vertices to load
	result = device->CreateBuffer(&vertexBufferDesc, NULL, &m_vertexBuffer);
	if (FAILED(result))
	{
		return false;
	}

	// Set up the description of the static index buffer.
	indexBufferDesc.Usage = D3D11_USAGE_DEFAULT;
	// each line is 2 vertices. each vert must be indexed.
	indexBufferDesc.ByteWidth = sizeof(unsigned long) * (MAX_LINES * 2);
	indexBufferDesc.BindFlags = D3D11_BIND_INDEX_BUFFER;
	indexBufferDesc.CPUAccessFlags = 0;
	indexBufferDesc.MiscFlags = 0;
	indexBufferDesc.StructureByteStride = 0;

	// Give the subresource structure a pointer to the index data.
	indexData.pSysMem = indices;
	indexData.SysMemPitch = 0;
	indexData.SysMemSlicePitch = 0;

	// Create the index buffer.
	// we populate it with the existing index data. this data will never change so we can load it here and never worry about it again.
	result = device->CreateBuffer(&indexBufferDesc, &indexData, &m_indexBuffer);
	if (FAILED(result))
	{
		return false;
	}

	delete[] indices;
	indices = 0;

	return true;
}

void LineBatcher::Shutdown()
{
	// Release the vertex and index buffers.
	ShutdownBuffers();

	// clean up our vertices.
	// indices were already cleaned up in the initialize function since they are not used outside of that scope.
	if (m_Vertices)
	{
		delete[] m_Vertices;
		m_Vertices = 0;
	}

	return;
}

void LineBatcher::ShutdownBuffers()
{
	// Release the index buffer.
	if (m_indexBuffer)
	{
		m_indexBuffer->Release();
		m_indexBuffer = 0;
	}

	// Release the vertex buffer.
	if (m_vertexBuffer)
	{
		m_vertexBuffer->Release();
		m_vertexBuffer = 0;
	}

	return;
}

void LineBatcher::Render(ID3D11DeviceContext* deviceContext)
{
	// NOTE:
	// the vertex buffer may contain vertices for old lines
	// this is why we pass an IndexCount to the DrawIndexed call in the ColorShaderClass
	// this is also why we must not call DrawIndexed if the IndexCount is 0 (pretty sure passing 0 to DrawIndexed is undefined).
	// i'm not sure if the vertex buffer uses GPU memory or sys memory. probably GPU memory.. so i should probably clean up old lines.
	// but mapping and unmapping just to zero some memory seems even more wasteful.

	// easy optimization
	if (m_NumLines == 0)
		return;

	// copy our active line's vertices to the vertex buffer
	PrepareBuffers(deviceContext);
	// Put the vertex and index buffers on the graphics pipeline to prepare them for drawing.
	RenderBuffers(deviceContext);

	return;
}

void LineBatcher::RenderBuffers(ID3D11DeviceContext* deviceContext)
{
	unsigned int stride;
	unsigned int offset;


	// Set vertex buffer stride and offset.
	stride = sizeof(VertexType);
	offset = 0;

	// Set the vertex buffer to active in the input assembler so it can be rendered.
	deviceContext->IASetVertexBuffers(0, 1, &m_vertexBuffer, &stride, &offset);

	// Set the index buffer to active in the input assembler so it can be rendered.
	deviceContext->IASetIndexBuffer(m_indexBuffer, DXGI_FORMAT_R32_UINT, 0);

	// Set the type of primitive that should be rendered from this vertex buffer, in this case lines.
	deviceContext->IASetPrimitiveTopology(D3D11_PRIMITIVE_TOPOLOGY_LINELIST);

	return;
}

bool LineBatcher::PrepareBuffers(ID3D11DeviceContext* deviceContext)
{
	// map vertex buffer
	// memcpy data from m_Vertices to mapped resource, length of (sizeof(VertexType) * (m_NumLines * 2))
	// unmap vertex buffer

	HRESULT result;
	D3D11_MAPPED_SUBRESOURCE mappedResource;
	VertexType* verticesPtr;

	// Lock the vertex buffer so it can be written to.
	result = deviceContext->Map(m_vertexBuffer, 0, D3D11_MAP_WRITE_DISCARD, 0, &mappedResource);
	if (FAILED(result))
	{
		return false;
	}

	// Get a pointer to the data in the vertex buffer.
	verticesPtr = (VertexType*)mappedResource.pData;

	// Copy the data into the vertex buffer.
	memcpy(verticesPtr, (void*)m_Vertices, (sizeof(VertexType) * (m_NumLines * 2)));

	// Unlock the vertex buffer.
	deviceContext->Unmap(m_vertexBuffer, 0);
}

bool LineBatcher::AddLine(D3DXVECTOR3 start, D3DXVECTOR3 end, D3DXVECTOR4 color)
{
	// write new VertexType to m_Vertices[m_NumLines] and m_Vertices[m_NumLines + 1]
	// increment m_NumLines
	m_Vertices[m_NumLines * 2].position = start;
	m_Vertices[m_NumLines * 2].color = color;

	m_Vertices[(m_NumLines * 2) + 1].position = end;
	m_Vertices[(m_NumLines * 2) + 1].color = color;

	m_NumLines++;

	return true;
}

void LineBatcher::ClearLines()
{
	m_NumLines = 0;
}

int LineBatcher::GetIndexCount()
{
	return m_NumLines * 2;
}