#pragma once

#define WIN32_LEAN_AND_MEAN             // Exclude rarely-used stuff from Windows headers
// Windows Header Files
#include <windows.h>
// C RunTime Header Files
#include <stdlib.h>
#include <malloc.h>
#include <memory.h>
#include <tchar.h>
#include <TlHelp32.h>
#include <stdio.h>
// for va_start, etc
#include <stdarg.h>

#include "../../../Common/AssaultCubeMemoryReader.h"
#include "../../../Common/assaultcube_wrapper.h"

#include "../../../Common/Input.h"

#include "ESPWindow_D3D11.h"

#define MAXSTRLEN 260
typedef char ac_string[MAXSTRLEN];
typedef wchar_t ac_wstring[MAXSTRLEN];

void EnableDebugPrivilege();
void GetGameWindow();
//void DrawPlayers(assaultcube_wrapper* game);
vec CalcAngle(vec src, vec dst);
void DoAimbot(assaultcube_wrapper* game);
void DoStatCheats(assaultcube_wrapper* game);
void DoFrame();
void CheckInput();
LRESULT CALLBACK WndProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam);
void Shutdown();