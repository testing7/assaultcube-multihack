#pragma once

#include <Windows.h>

#include <wingdi.h>
#include <gl/GL.h>
#include <gl/GLU.h>

#pragma comment(lib,"opengl32.lib")
#pragma comment(lib,"glu32.lib")

#include "../../Common/EngineStructs.h"

class ESPWindow
{
public:
	// registers the window class
	ESPWindow(HINSTANCE instance, LPCWSTR name, LRESULT(__stdcall* globalWndProc)(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam), void(*dOnFrame)(void), void(*dOnDestroy)(void));

private:
	HINSTANCE Instance = NULL;
	LPCWSTR WindowName;
	HWND HWindow = NULL;
	HDC MainHDC = NULL;
	HGLRC MainGLRenderingContext = NULL;
	HWND GameWindow = NULL;
	LRESULT(__stdcall* GlobalWndProc)(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam);
	void(*OnFrame)(void);
	void(*OnDestroy)(void);

public:
	RECT GameWindowRect;
	RECT GameClientRect;
	float GameWidth, GameHeight;
	float ViewportWidth, ViewportHeight, HalfVPWidth, HalfVPHeight;

private:
	bool SetupOpenGL();
	ATOM DoRegisterClass(HINSTANCE hInstance);
	
public:
	// creates the window
	bool Initialize(int nCmdShow);
	void ResizeWindow();
	// windows message pump
	MSG Run();
	// windows message handler
	LRESULT CALLBACK WndProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam);

	void SetGameWindow(HWND gameWindow);
	void SwitchToOrthoRendering();
	void SwitchToPerspectiveRendering(float fov, float screenWidth, float screenHeight, vec& camPos, vec& camAngles);
	bool WorldToOrthoScreen(glmatrixf& mvpMat, float virtW, float virtH, vec* worldCoords, vec& outScreenCoords);
	void drawstring_2d(const char* str, float x, float y, vec4* color);
	void drawstringf_2d(const char* format, int x, int y, vec4* color, ...);
	void box2d(float x1, float y1, float x2, float y2, vec4* color);
	void outline2d(float x1, float y1, float x2, float y2, vec4* color);


};

