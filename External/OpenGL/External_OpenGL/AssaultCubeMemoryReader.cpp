#include "AssaultCubeMemoryReader.h"

#include "../../Common/assaultcube_wrapper.h"

bool AssaultCubeMemoryReader::HasRanOnce = false;
assaultcube_wrapper* AssaultCubeMemoryReader::assaultcube_main = 0;

void AssaultCubeMemoryReader::InitializeForProcess(HANDLE proc)
{
	ExternalMemoryReader::InitializeForProcess(proc);
	// already forgot what i was going to do here..
}

void AssaultCubeMemoryReader::WriteHealthAndArmor()
{
	if (IsStillRunning() == false)
		return;
	//assaultcube_main->localplayer
	DWORD localPlayerAddr = ResolvePointerChain(2, 0x10F4F4, 0x0);
	if (localPlayerAddr == 0) return;
	DWORD healthOffset = (DWORD)&assaultcube_main->localplayer->health - (DWORD)assaultcube_main->localplayer;
	DWORD healthAddr = localPlayerAddr + healthOffset;
	int buf[2] = { 100, 100 };
	int numBytesWritten = 0;
	if (!WriteDirect(healthAddr, buf, 8))
	{
		MessageBox(NULL, L"WPM Failed (health & armor)", L"Assault Cube Multihack - External", 0);
	}
}

void AssaultCubeMemoryReader::WriteAmmo()
{
	if (IsStillRunning() == false)
		return;
	// rather than reading which weapon is being used and setting the corresponding ammo, we'll just set all ammo for all weapons since the game conveniently keeps all ammo counts in one big int array
	DWORD localPlayerAddr = ResolvePointerChain(2, 0x10F4F4, 0x0);
	if (localPlayerAddr == 0) return;
	DWORD magOffset = (DWORD)&assaultcube_main->localplayer->mag[0] - (DWORD)assaultcube_main->localplayer;
	DWORD magAddr = localPlayerAddr + magOffset;
	// let's just set our local copy of the ammo array to 100 and then copy it over to the process
	for (int index = 0; index < NUMGUNS; index++)
		assaultcube_main->localplayer->mag[index] = 100;
	int numBytesWritten = 0;
	if (!WriteDirect(magAddr, assaultcube_main->localplayer->mag, sizeof(assaultcube_main->localplayer->mag)))
	{
		MessageBox(NULL, L"WPM Failed (ammmo)", L"Assault Cube Multihack - External", 0);
	}
}

void AssaultCubeMemoryReader::WriteGunInfo()
{
	if (IsStillRunning() == false)
		return;
	for (int index = 0; index < NUMGUNS; index++)
	{
		assaultcube_main->guninfos[index].pushfactor = 0;
		assaultcube_main->guninfos[index].recoil = 0;
		assaultcube_main->guninfos[index].recoilbase = 0;
		assaultcube_main->guninfos[index].maxrecoil = 0;
		assaultcube_main->guninfos[index].spread = 0;
		//myWeapon->info->attackdelay = 0; // gun go brrrrrt. actually not a great idea. will expend entire mag in a single frame.
		assaultcube_main->guninfos[index].attackdelay = 15;
		// totally ruins the fun
		//assaultcube_main->guninfos->damage = 200;
	}
	DWORD guninfoAddr = (ProcessBaseAddress + 0xFC300);
	if (!WriteDirect(guninfoAddr, assaultcube_main->guninfos, sizeof(assaultcube_main->guninfos)))
	{
		MessageBox(NULL, L"WPM Failed (guninfo)", L"Assault Cube Multihack - External", 0);
	}
}

void AssaultCubeMemoryReader::RestoreGunInfo()
{
	if (IsStillRunning() == false)
		return;
	DWORD guninfoAddr = (ProcessBaseAddress + 0xFC300);
	int numBytesWritten = 0;
	if (!WriteDirect(guninfoAddr, assaultcube_main->guninfos_original, sizeof(assaultcube_main->guninfos_original)))
	{
		MessageBox(NULL, L"WPM Failed (restore_guninfo)", L"Assault Cube Multihack - External", 0);
	}
}

void AssaultCubeMemoryReader::WriteYawAndPitch(float yaw, float pitch)
{
	if (IsStillRunning() == false)
		return;
	DWORD localPlayerAddr = ResolvePointerChain(2, 0x10F4F4, 0x0);
	if (localPlayerAddr == 0) return;
	DWORD yawOffset = (DWORD)&assaultcube_main->localplayer->yaw - (DWORD)assaultcube_main->localplayer;
	DWORD yawAddr = localPlayerAddr + yawOffset;
	float buf[2] = { yaw, pitch };
	if(!WriteDirect(yawAddr, buf, 8))
	{
		MessageBox(NULL, L"WPM Failed (yaw & pitch)", L"Assault Cube Multihack - External", 0);
	}
}

assaultcube_wrapper* AssaultCubeMemoryReader::Update()
{
	// check if process has exited, if so return NULL and handle it in main
	if (IsStillRunning() == false)
		return NULL;

	if (HasRanOnce == false)
	{
		assaultcube_main = new assaultcube_wrapper;
		// TO-DO:
		// read all the guninfo structs (1 RPM)
		DWORD guninfoAddr = (ProcessBaseAddress + 0xFC300);
		if (!ReadDirect(guninfoAddr, assaultcube_main->guninfos, sizeof(assaultcube_main->guninfos))) return NULL;
		// make a copy of this for if we ever want to modify and then restore it
		memcpy(&assaultcube_main->guninfos_original, &assaultcube_main->guninfos, sizeof(assaultcube_main->guninfos));
		HasRanOnce = true;
	}
	else
	{
		// should delete allocated weapon for each player/bot, preventing memory leak
		assaultcube_main->players->clear();
		assaultcube_main->bots->clear();
	}
	// read scr_w & scr_h
	if (!ReadDirect((ProcessBaseAddress + 0x110C94), &assaultcube_main->scr_w, 8)) return NULL;
	// read VIRTW
	if (!ReadValue((ProcessBaseAddress + 0x102574), &assaultcube_main->VIRTW)) return NULL;
	// read VIRTH
	if (!ReadValue((ProcessBaseAddress + 0xEE5E8), &assaultcube_main->VIRTH)) return NULL;
	// read fov, scopeFOV, spectatorFOV
	if (!ReadDirect((ProcessBaseAddress + 0x10F1C4), &assaultcube_main->fov, 12)) return NULL;
	// read mvpMatrix
	if (!ReadValue((ProcessBaseAddress + 0x101AE8), &assaultcube_main->mvpmatrix)) return NULL;
	// BEGIN DEBUG
	if (!ReadValue((ProcessBaseAddress + 0x101B28), &assaultcube_main->mvMatrix)) return NULL;
	if (!ReadValue((ProcessBaseAddress + 0x101BB8), &assaultcube_main->projMatrix)) return NULL;
	// END DEBUG
	// read localPlayer
	DWORD localPlayerAddr = ResolvePointerChain(2, 0x10F4F4, 0x0);
	if (localPlayerAddr == 0) return NULL;
	if (!ReadValue(localPlayerAddr, assaultcube_main->localplayer)) return NULL;
	// read camera
	DWORD cameraAddr = ResolvePointerChain(2, 0x109B74, 0x0);
	if (cameraAddr == 0) return NULL;
	if (!ReadValue(cameraAddr, assaultcube_main->maincamera)) return NULL;

	if (!ReadVecContainer((ProcessBaseAddress + 0x10F4F8), *assaultcube_main->players)) return NULL;
	if (!ReadVecContainer((ProcessBaseAddress + 0x110D90), *assaultcube_main->bots)) return NULL;

	return assaultcube_main;
}

