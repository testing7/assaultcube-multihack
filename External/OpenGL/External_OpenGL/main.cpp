#include "main.h"

// NOTE:
// FOR DEBUGMATRIXES() ONLY, remove later!
#include <sstream>

LPCWSTR WindowName = L"Assault Cube Multihack - External";
LPCWSTR GameExeName = L"ac_client.exe";
HINSTANCE myInstance = NULL;
ESPWindow *espWindow = NULL;

HWND GameWindow = NULL;
HANDLE GameHandle = NULL;

bool shouldDoStatCheats = false;
bool hasSetGunInfoStats = false;
playerent_wrapper* aimbotTargetPlayer = NULL;

void GetGameWindow()
{
    while (GameHandle == NULL)
    {
        HANDLE procSnapshot = CreateToolhelp32Snapshot(TH32CS_SNAPPROCESS, 0);
        if (procSnapshot == INVALID_HANDLE_VALUE)
        {
            // TO-DO:
            // error reporting
            continue;
        }
        PROCESSENTRY32 pe32;
        pe32.dwSize = sizeof(PROCESSENTRY32);
        if (Process32First(procSnapshot, &pe32) == FALSE)
        {
            CloseHandle(procSnapshot);
            // TO-DO:
            // error reporting
            continue;
        }
        do
        {
            if (_wcsicmp(pe32.szExeFile, L"ac_client.exe") == 0)
            {
                // found game, get handle and exit loop
                GameHandle = OpenProcess(PROCESS_ALL_ACCESS, FALSE, pe32.th32ProcessID);
                if (GameHandle == NULL)
                {
                    break;
                    // TO-DO:
                    // error reporting
                }
            }
        } while (Process32Next(procSnapshot, &pe32));

        CloseHandle(procSnapshot);
        Sleep(100);
    }
    while (GameWindow == NULL)
    {
        GameWindow = FindWindow(NULL, L"AssaultCube");
        Sleep(100);
    }
}

void EnableDebugPrivilege()
{
    HANDLE hToken = NULL;
    TOKEN_PRIVILEGES tokenPriv;
    LUID luidDebug;
    printf("Enabling debug privilege...\n");
    if (OpenProcessToken(GetCurrentProcess(), TOKEN_ADJUST_PRIVILEGES, &hToken) != FALSE)
    {
        if (LookupPrivilegeValue(NULL, SE_DEBUG_NAME, &luidDebug) != FALSE)
        {
            tokenPriv.PrivilegeCount = 1;
            tokenPriv.Privileges[0].Luid = luidDebug;
            tokenPriv.Privileges[0].Attributes = SE_PRIVILEGE_ENABLED;
            if (AdjustTokenPrivileges(hToken, FALSE, &tokenPriv, 0, NULL, NULL) != FALSE)
            {
                // Always successful, even in the cases which lead to OpenProcess failure
                printf("Successfully enabled debug privilege\n");
            }
            else
            {
                printf("Failed to enable debug privilege (GetLastError(): %d)\n", GetLastError());
            }
        }
        else
        {
            printf("Failed to LookupPrivilegeValue\n");
        }
    }
    else
    {
        printf("Failed to OpenProcessToken\n");
    }
    printf("Closing token handle... Done.\n");
    CloseHandle(hToken);
}

void DrawMatrix(glmatrixf matrix, int x, int y, vec4* color)
{
    espWindow->drawstringf_2d("%.2f %.2f %.2f %.2f", x, y, color, matrix[0], matrix[1], matrix[2], matrix[3]);
    espWindow->drawstringf_2d("%.2f %.2f %.2f %.2f", x, y + 15, color, matrix[4], matrix[5], matrix[6], matrix[7]);
    espWindow->drawstringf_2d("%.2f %.2f %.2f %.2f", x, y + 30, color, matrix[8], matrix[9], matrix[10], matrix[11]);
    espWindow->drawstringf_2d("%.2f %.2f %.2f %.2f", x, y + 45, color, matrix[12], matrix[13], matrix[14], matrix[15]);
}

void DebugTransformation(vec pos, glmatrixf matrix, vec& screenPos, float halfVPWidth, float halfVPHeight, int debugDrawX, int debugDrawY, vec4* debugDrawColor)
{
    vec4 clipCoords;
    clipCoords.x = pos.x * matrix[0] + pos.y * matrix[4] + pos.z * matrix[8] + matrix[12];
    clipCoords.y = pos.x * matrix[1] + pos.y * matrix[5] + pos.z * matrix[9] + matrix[13];
    clipCoords.z = pos.x * matrix[2] + pos.y * matrix[6] + pos.z * matrix[10] + matrix[14];
    clipCoords.w = pos.x * matrix[3] + pos.y * matrix[7] + pos.z * matrix[11] + matrix[15];

    if (clipCoords.w < 0.1f)
    {
        espWindow->drawstringf_2d("FAILED [clip: (%.2f, %.2f, %.2f, %.2f)]", debugDrawX, debugDrawY, debugDrawColor, clipCoords.x, clipCoords.y, clipCoords.z, clipCoords.w);
        return;
    }

    //perspective division, dividing by clip.W = Normalized Device Coordinates
    vec NDC;
    NDC.x = clipCoords.x / clipCoords.w;
    NDC.y = clipCoords.y / clipCoords.w;
    NDC.z = clipCoords.z / clipCoords.w;

    //Transform to window coordinates
    screenPos.x = (NDC.x * halfVPWidth) + (NDC.x + halfVPWidth);
    screenPos.y = -(NDC.y * halfVPHeight) + (NDC.y + halfVPHeight);
    espWindow->drawstringf_2d("SUCCESS [screen: (%.2f, %.2f)]", debugDrawX, debugDrawY, debugDrawColor, screenPos.x, screenPos.y);
}

void DebugMatrices(assaultcube_wrapper* game)
{
    espWindow->SwitchToOrthoRendering();
    if ((game->bots == NULL) || (game->bots->size() < 1)) return;
    playerent_wrapper player = (playerent_wrapper)game->bots->at(0);

    // transform world coords to screen coords using just projection matrix (won't work)
    vec4 pMatColor = vec4(1.f, 0.f, 0.f, 1.f);
    espWindow->drawstring_2d("=== Projection Matrix===", 310, 50, &pMatColor);
    DrawMatrix(game->projMatrix, 310, 65, &pMatColor);
    vec screenPos_FromPMatrix = vec(0.f, 0.f, 0.f);
    DebugTransformation(player.origin, game->projMatrix, screenPos_FromPMatrix, espWindow->HalfVPWidth, espWindow->HalfVPHeight, 310, 155, &pMatColor);
    //espWindow->outline2d(screenPos_FromPMatrix.x - 10.f, screenPos_FromPMatrix.y - 10.f, screenPos_FromPMatrix.x + 10.f, screenPos_FromPMatrix.y + 10.f, &pMatColor);

    // transform worlds coords to screen coords using just view matrix (won't work)
    vec4 vMatColor = vec4(0.f, 1.f, 0.f, 1.f);
    espWindow->drawstring_2d("=== View Matrix===", 10, 50, &vMatColor);
    DrawMatrix(game->mvMatrix, 10, 65, &vMatColor);
    vec screenPos_FromVMatrix = vec(0.f, 0.f, 0.f);
    DebugTransformation(player.origin, game->mvMatrix, screenPos_FromVMatrix, espWindow->HalfVPWidth, espWindow->HalfVPHeight, 10, 155, &vMatColor);
    //espWindow->outline2d(screenPos_FromVMatrix.x - 10.f, screenPos_FromVMatrix.y - 10.f, screenPos_FromVMatrix.x + 10.f, screenPos_FromVMatrix.y + 10.f, &vMatColor);

    // transform world coords to screen coords using mvp matrix (does work)
    vec4 mvpMatColor = vec4(1.f, 0.f, 1.f, 1.f);
    espWindow->drawstring_2d("=== MVP Matrix===", 710, 50, &mvpMatColor);
    DrawMatrix(game->mvpmatrix, 710, 65, &mvpMatColor);
    vec screenPos_FromMVPMatrix = vec(0.f, 0.f, 0.f);
    DebugTransformation(player.origin, game->mvpmatrix, screenPos_FromMVPMatrix, espWindow->HalfVPWidth, espWindow->HalfVPHeight, 710, 155, &mvpMatColor);
    //espWindow->outline2d(screenPos_FromMVPMatrix.x - 10.f, screenPos_FromMVPMatrix.y - 10.f, screenPos_FromMVPMatrix.x + 10.f, screenPos_FromMVPMatrix.y + 10.f, &mvpMatColor);
}

void DrawPlayers(assaultcube_wrapper* game)
{
    
    for (std::vector<botent_wrapper>::iterator it = game->bots->begin(); it != game->bots->end(); ++it)
    {
        playerent_wrapper player = (playerent_wrapper)*it;
        if (player.state != 0x0) continue;

        vec4 color = vec4(1.f, 0.f, 0.f, 1.f);
        // hopefully the address is the address within the vector's buffer, and not the address of the iterator struct.. pretty bad way of doing this overall. don't be me.
        if (&player == aimbotTargetPlayer)
            color = vec4(1.0f, 0.0f, 1.0f, 1.f);
        if (player.team == game->localplayer->team)
            color = vec4(0.f, 1.f, 0.f, 0.3f);

        /* commenting this out for now, perspective rendering is too OP
        SwitchToOrthoRendering(game);
        vec screenPos = vec(0.f, 0.f, 0.f);
        if (WorldToOrthoScreen(game, &player.origin, screenPos))
        {
            if (player.team == game->localplayer->team)
                drawstring_2d(player.name, screenPos.x, screenPos.y, &color);
            else
                drawstring_2d(player.name, screenPos.x, screenPos.y, &color);
        }
        */
        vec camAngles = vec(game->maincamera->yaw, game->maincamera->pitch, game->maincamera->roll);
        espWindow->SwitchToPerspectiveRendering(game->fov, (float)game->scr_w, (float)game->scr_h, game->maincamera->origin, camAngles);
        // in openGL we can just translate to the player's position and draw - eliminating the need for world to screen calculations and distance scaling
        // culling is also automatically performed per-vertex (vertices not present on the screen are discarded - including text)
        // i wonder if directx can do this too.. i've never really tried
        // i also wonder if there's a notable performance penalty for all the translating / rotating, and pushing / popping matrices, or if openGL optimizes all that under the hood
        // i'm definitely not noticing any performance issues though
        glPushMatrix();
        glTranslatef(player.origin.x, player.origin.y, player.origin.z);
        // makes the box always face the player. not fully necessary, but aesthetically pleasing.
        glRotatef(game->maincamera->yaw, 0, 0, 1);
        glRotatef(-90, 1, 0, 0);

        // ESP box
        espWindow->outline2d(-player.radius, 0, player.radius, player.eyeheight, &color);
        // nametag
        espWindow->drawstring_2d(player.name, -player.radius, player.eyeheight - 5, &color);

        glPopMatrix();

    }
    // DEBUG:
    DebugMatrices(game);
}

// credit: Rake via [https://guidedhacking.com/threads/ultimate-calcangle-thread-how-to-calculate-angle-functions.8165/#post-43092]
vec CalcAngle(vec src, vec dst)
{
    vec angle;
    float dist = src.dist(dst);

    // pitch, yaw, roll
    angle.x = asinf((dst.z - src.z) / dist) * Rad2Deg;
    angle.y = -atan2f(dst.x - src.x, dst.y - src.y) / PI * 180.0f + 180.0f;
    angle.z = 0.0f;

    return angle;
}

void DoAimbot(assaultcube_wrapper* game)
{
    if (game->localplayer->state != 0x0)
        return;
    vec myEyePos = game->localplayer->origin;
    myEyePos.z += game->localplayer->eyeheight;
    int myTeam = game->localplayer->team;
    vec myHeading = vec(game->localplayer->pitch, game->localplayer->yaw, 0.f);

    float closestToCrosshairDiff = 3.402823466e+38F;
    vec closestAngle = vec(3.402823466e+38F, 3.402823466e+38F, 0.f);
    aimbotTargetPlayer = NULL;
    for (std::vector<botent_wrapper>::size_type index = 0; index < game->bots->size(); index++)
    {
        botent_wrapper player = (botent_wrapper)game->bots->at(index);
        if ((player.state != 0x0) || (player.team == myTeam)) continue;
        vec enemyEyePos = player.origin;
        enemyEyePos.z += player.eyeheight;
        vec angle = CalcAngle(myEyePos, enemyEyePos);
        float diff = myHeading.dist(angle);
        if (diff < closestToCrosshairDiff)
        {
            closestToCrosshairDiff = diff;
            closestAngle = angle;
            // i don't like this but it'll do for now
            aimbotTargetPlayer = &game->bots[0][index];
        }
    }
    if (aimbotTargetPlayer != NULL)
    {
        // yaw, pitch, roll
        AssaultCubeMemoryReader::WriteYawAndPitch(closestAngle.y, closestAngle.x);
    }
}

void DoStatCheats(assaultcube_wrapper* game)
{
    if (game->localplayer->state != 0x0)
        return;
    AssaultCubeMemoryReader::WriteHealthAndArmor();
    AssaultCubeMemoryReader::WriteAmmo();
    if (hasSetGunInfoStats == false)
    {
        hasSetGunInfoStats = true;
        AssaultCubeMemoryReader::WriteGunInfo();
    }
}

void DoFrame()
{
    assaultcube_wrapper* game = AssaultCubeMemoryReader::Update();
    if (game == NULL)
    {
        // TO-DO:
        // start polling for process again, then open handle to new process and call AssaultCubeMemoryReader::InitializeForProcess().
        return;
    }

    CheckInput();

    // ESP-related stuff (read-only)
    glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
    glEnable(GL_BLEND);

    espWindow->SwitchToOrthoRendering();
    // begin debug
    vec4 color = vec4(1.f, 1.f, 0.f, 1.f);
    espWindow->drawstringf_2d("LocalPlayer: %x\nNumBots: %d", 1, 400, &color, game->localplayer, (int)game->bots->size());
    // end debug

    DrawPlayers(game);

    glDisable(GL_BLEND);

    // aimbot & stat-related stuff (read & write)
    if (Input::IsKeyPressed(VK_LBUTTON, true))
    {
        DoAimbot(game);
    }
    if (shouldDoStatCheats)
    {
        DoStatCheats(game);
    }
}

void CheckInput()
{
    if (Input::IsKeyPressed(VK_NUMPAD1))
    {
        shouldDoStatCheats = !shouldDoStatCheats;
    }
    if (Input::IsKeyPressed(VK_NUMPAD2))
    {
        espWindow->ResizeWindow();
    }
    // TO-DO:
    // all input handling here, except the mouse-button aimbot
}

LRESULT CALLBACK WndProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam)
{
    // if we had more than one window we'd sort and pass it to the appropriate window's WndProc
    return espWindow->WndProc(hWnd, message, wParam, lParam);
}

void Shutdown()
{
    CloseHandle(GameHandle);
    CloseHandle(GameWindow);
}

int APIENTRY wWinMain(_In_ HINSTANCE hInstance,
    _In_opt_ HINSTANCE hPrevInstance,
    _In_ LPWSTR    lpCmdLine,
    _In_ int       nCmdShow)
{
    myInstance = hInstance;
    MSG msg;
    GetGameWindow();
    // initialize memory reader
    AssaultCubeMemoryReader::InitializeForProcess(GameHandle);

    espWindow = new ESPWindow(myInstance, L"Assault Cube Multihack - External (OpenGL)", WndProc, DoFrame, Shutdown);
    if (!espWindow->Initialize(nCmdShow))
    {
        MessageBox(NULL, L"Could not initialize overlay", L"Assault Cube Multihack - External (OpenGL)", MB_ICONERROR);
    }
    espWindow->SetGameWindow(GameWindow);

     msg = espWindow->Run();

    delete espWindow;
    espWindow = 0;

    return (int)msg.wParam;
}