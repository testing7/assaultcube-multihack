#pragma once

#include <Windows.h>

namespace utils
{
	void ErrorMsgBoxW(DWORD errCode, HWND hwnd, LPCWSTR msgPrefix, LPCWSTR caption, UINT type);
}