#include "utils.h"
#include <stdio.h>

namespace utils
{
	void ErrorMsgBoxW(DWORD errCode, HWND hwnd, LPCWSTR msgPrefix, LPCWSTR caption, UINT type)
	{
		wchar_t buf[255];
		swprintf_s(buf, 255, L"%s: 0x%d", msgPrefix, errCode);
		MessageBoxW(hwnd, buf, caption, type);
	}
}