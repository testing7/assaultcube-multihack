#include "ESPWindow.h"

ESPWindow::ESPWindow(HINSTANCE instance, LPCWSTR name, LRESULT(__stdcall* globalWndProc)(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam), void(*dOnFrame)(void), void(*dOnDestroy)(void))
{
    Instance = instance;
    WindowName = name;
    GlobalWndProc = globalWndProc;
    OnFrame = dOnFrame;
    OnDestroy = dOnDestroy;

    DoRegisterClass(Instance);
}

bool ESPWindow::Initialize(int nCmdShow)
{
    // credit:
    // code for initializing the ESPWindow is inspired by Puddin Poppin via [https://www.unknowncheats.me/forum/counterstrike-global-offensive/141824-opengl-esp-source.html]
    HWindow = CreateWindowEx(
        WS_EX_TRANSPARENT | WS_EX_TOPMOST | WS_EX_LAYERED,
        WindowName,
        WindowName,
        WS_POPUP | WS_VISIBLE | WS_MAXIMIZE,
        0,
        0,
        1,
        1,
        NULL, NULL, Instance, NULL);

    if (HWindow == NULL)
    {
        UnregisterClass(WindowName, Instance);
        MessageBox(0, L"Window Creation Failed!", L"Error!", MB_OK);
        return false;
    }

    // sets the color transparency key to 0,0,0 (any pixel drawn with this color is transparent)
    // also sets the opacity of the window to 255 (opaque)
    if (SetLayeredWindowAttributes(HWindow, RGB(0, 0, 0), 255, LWA_COLORKEY | LWA_ALPHA) == 0)
    {
        UnregisterClass(WindowName, Instance);
        MessageBox(0, L"Failed to set layered window attributes!", L"Error", MB_ICONERROR | MB_OK);
        return false;
    }

    // as of win8, DWM composition is always enabled, so we can skip this
    /*
    BOOL IsEnabled;
    if (DwmIsCompositionEnabled(&IsEnabled) != S_OK)
    {
        MessageBox(0, L"Failed to check if DWM window composition is enabled! Assuming false!", L"Error", MB_ICONERROR | MB_OK);
        UnregisterClass(WindowName, Instance);
        //ExitThread(0);
    }
    */
    // note: this works while commented out. i'm guessing it's a relic of windows vista, where aero was first rolled out (blurring)
    /* skipping this for now too, i just want to see what happens
    //the region that is not blurred is rendered based off the alpha channel, therefore, we create an invalid region so that nothing is blurred but the alpha blending remains. We need this due to... who the fuck knows why layered window attributes wont work? seriously tell me please :'(
    DWM_BLURBEHIND bb = { DWM_BB_ENABLE | DWM_BB_BLURREGION, true, CreateRectRgn(0, 0, -1, -1), true };
    //enable trasnparency via dwm
    //NOTE: SOME WINDOWS VERSIONS DO NOT SUPPORT THIS
    if (DwmEnableBlurBehindWindow(HWindow, &bb) != S_OK)
    {
        MessageBox(0, L"Failed to enable DWM window blur!", L"Error", MB_ICONERROR | MB_OK);
        UnregisterClass(WindowName, Instance);
        //ExitThread(0);
    }
    */
    ShowWindow(HWindow, nCmdShow);
    // sends paint event directly to wndproc, bypassing queue
    UpdateWindow(HWindow);
    return true;
}

//resize and reposition window
void ESPWindow::ResizeWindow()
{
    if (GameWindow == NULL) return;

    //get physical window bounds
    GetWindowRect(GameWindow, &GameWindowRect);
    //get logical client bounds
    GetClientRect(GameWindow, &GameClientRect);
    //width and height of client rect
    GameWidth = GameClientRect.right - GameClientRect.left;
    GameHeight = GameClientRect.bottom - GameClientRect.top;

    float WindowWidth = GameWindowRect.right - GameWindowRect.left;
    float WindowHeight = GameWindowRect.bottom - GameWindowRect.top;
    // the frame of the window (if window is decorated as such. no way of getting the actual value, so we approximate) - should only be like 2 pixels.
    float frameWidth = (WindowWidth - GameWidth) * 0.5f;
    // again, more window decoration. i think a tiny frame is added between the titlebar and the game rect. not entirely sure. close enough either way.
    float titlebarHeight = (WindowHeight - GameHeight);
    ViewportWidth = WindowWidth - (frameWidth * 2);
    ViewportHeight = WindowHeight - titlebarHeight - frameWidth;
    HalfVPWidth = ViewportWidth * 0.5f;
    HalfVPHeight = ViewportHeight * 0.5f;

    if (HWindow != NULL)
    {
        SetWindowPos(HWindow, 0, GameWindowRect.left + frameWidth, GameWindowRect.top + titlebarHeight, WindowWidth - (frameWidth * 2), WindowHeight - titlebarHeight - frameWidth, 0);
        glViewport(0, 0, ViewportWidth, ViewportHeight);
        // NOTE:
        // don't necessarily have to set up ortho proj here. will probably move to rendering loop later (since we'll be switching back and forth between ortho and perspective)
        glOrtho(0, ViewportWidth, ViewportHeight, 0, 0, 1);
    }
}

bool ESPWindow::SetupOpenGL()
{
    PIXELFORMATDESCRIPTOR pfd = {
        sizeof(PIXELFORMATDESCRIPTOR),
        1,                                  // Version Number
        PFD_DRAW_TO_WINDOW |              // Format Must Support Window
        PFD_SUPPORT_OPENGL |              // Format Must Support OpenGL
//        PFD_SUPPORT_GDI|                  // Format Must Support GDI
        PFD_SUPPORT_COMPOSITION |         // Format Must Support Composition
        PFD_DOUBLEBUFFER,                 // Must Support Double Buffering
        PFD_TYPE_RGBA,                    // Request An RGBA Format
        32,                               // Select Our Color Depth
        0, 0, 0, 0, 0, 0,                 // Color Bits Ignored
        8,                                // An Alpha Buffer
        0,                                // Shift Bit Ignored
        0,                                // No Accumulation Buffer
        0, 0, 0, 0,                       // Accumulation Bits Ignored
        0 ,                               // No Z-Buffer (Depth Buffer)
        8,                                // Some Stencil Buffer
        0,                                // No Auxiliary Buffer
        PFD_MAIN_PLANE,                   // Main Drawing Layer
        0,                                // Reserved
        0, 0, 0                           // Layer Masks Ignored
    };
    int PixelFormat = ChoosePixelFormat(MainHDC, &pfd);
    if (!PixelFormat)
    {
        MessageBox(0, L"Failed to get pixel format!", L"Error", MB_ICONERROR | MB_OK);
        return false;
    }
    if (!SetPixelFormat(MainHDC, PixelFormat, &pfd))
    {
        MessageBox(0, L"Failed to set pixel format!", L"Error", MB_ICONERROR | MB_OK);
        return false;
    }
    MainGLRenderingContext = wglCreateContext(MainHDC);
    if (MainGLRenderingContext == NULL)
    {
        MessageBox(0, L"Failed to create rendering context!", L"Error", MB_ICONERROR | MB_OK);
        return false;
    }
    if (wglMakeCurrent(MainHDC, MainGLRenderingContext) == FALSE)
    {
        MessageBox(0, L"Failed to set rendering context!", L"Error", MB_ICONERROR | MB_OK);
        return false;
    }
    // set up fonts
    SelectObject(MainHDC, GetStockObject(ANSI_FIXED_FONT));
    wglUseFontBitmaps(MainHDC, 0, 255, 1000);

    glMatrixMode(GL_PROJECTION);
    //color to set when screen is cleared, black with no alpha
    glClearColor(0.f, 0.f, 0.f, 0.f);

    ResizeWindow();
}

ATOM ESPWindow::DoRegisterClass(HINSTANCE hInstance)
{
    WNDCLASSEXW wcex;
    wcex.cbSize = sizeof(WNDCLASSEX);

    //wcex.style = CS_HREDRAW | CS_VREDRAW;
    wcex.style = 0;
    wcex.lpfnWndProc = GlobalWndProc;
    wcex.cbClsExtra = 0;
    wcex.cbWndExtra = 0;
    wcex.hInstance = hInstance;
    wcex.hIcon = NULL;
    wcex.hCursor = NULL;
    //wcex.hbrBackground = (HBRUSH)(COLOR_WINDOW + 1);
    wcex.hbrBackground = CreateSolidBrush(RGB(0, 0, 0));
    wcex.lpszMenuName = NULL;
    wcex.lpszClassName = WindowName;
    wcex.hIconSm = NULL;

    return RegisterClassExW(&wcex);
}

LRESULT CALLBACK ESPWindow::WndProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam)
{
    switch (message)
    {
    case WM_CREATE:
    {
        MainHDC = GetDC(hWnd);
        SetupOpenGL();
        return 0;
    }
    case WM_CLOSE:
    {
        DestroyWindow(hWnd);
        break;
    }
    case WM_PAINT:
    {
        PAINTSTRUCT ps;
        HDC hdc = BeginPaint(hWnd, &ps);
        // TODO: Add any drawing code that uses hdc here...
        glClear(GL_COLOR_BUFFER_BIT);

        OnFrame();

        SwapBuffers(MainHDC);
        EndPaint(hWnd, &ps);
    }
    break;
    case WM_DESTROY:
        wglMakeCurrent(NULL, NULL);
        wglDeleteContext(MainGLRenderingContext);
        ReleaseDC(hWnd, MainHDC);
        UnregisterClass(WindowName, Instance);

        // main.cpp should close all open handles in this OnDestroy() callback.
        OnDestroy();
        // i don't think it's necessary to close handles to your own stuff, right? they should just be pseudo handles
        // plus all our objects are being destroyed at this point anyway, so all handles should be released by the OS, right?

        PostQuitMessage(0);
        break;
    default:
        return DefWindowProc(hWnd, message, wParam, lParam);
    }
    return 0;
}

MSG ESPWindow::Run()
{
    // main loop, using PeekMessage and processing entire queue, THEN invalidating client rect to generate a redraw msg.
    // can add timing to constrain the redraws to a certain FPS, too
    MSG msg;
    while (true)
    {
        // message pump, always fully clear so windows doesn't think we're frozen
        while (PeekMessage(&msg, NULL, 0, 0, PM_REMOVE))
        {
            TranslateMessage(&msg);
            DispatchMessage(&msg);
            if (msg.message == WM_QUIT)
                return msg;
        }
        // main rendering loop:
        // invalidate rect to indirectly issue a WM_PAINT message
        InvalidateRect(HWindow, NULL, false);
        Sleep(5);
    }
    // we are shutting down now, so return to WinMain (and then return from there)
    return msg;
}

void ESPWindow::SetGameWindow(HWND gameWindow)
{
    GameWindow = gameWindow;
    ResizeWindow();
}

void ESPWindow::SwitchToOrthoRendering()
{
    // clear all the matrices
    glMatrixMode(GL_MODELVIEW);
    glLoadIdentity();
    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();

    // create ortho perspective for drawing HUD text
    glOrtho(0, ViewportWidth, ViewportHeight, 0, -1, 1);
    glEnable(GL_TEXTURE_2D);
    // now we can draw in screenspace
}

void ESPWindow::SwitchToPerspectiveRendering(float fov, float screenWidth, float screenHeight, vec& camPos, vec& camAngles)
{
    // reconstruct our 3d projection matrix
    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();
    float aspect = screenWidth / screenHeight;
    float fovy = 2 * atan2(tan((double)fov / 2 * RAD), aspect) / RAD;
    float nearplane = 0.15f;
    float farplane = 450.f;

    double ydist = nearplane * tan((double)fovy / 2 * RAD);
    double xdist = ydist * aspect;
    glFrustum(-xdist, xdist, -ydist, ydist, nearplane, farplane);

    // now set up our mv matrix
    glMatrixMode(GL_MODELVIEW);
    glLoadIdentity();
    // camAngles are in yaw, pitch, roll layout
    glRotatef(camAngles.z, 0, 0, 1); // roll (z)
    glRotatef(camAngles.y, -1, 0, 0); // pitch (y)
    glRotatef(camAngles.x, 0, 1, 0); // yaw (x)

    // move from RH to Z-up LH quake style worldspace
    glRotatef(-90, 1, 0, 0);
    glScalef(1, -1, 1);

    glTranslatef(-camPos.x, -camPos.y, -camPos.z);

    glDisable(GL_TEXTURE_2D);
    // now we can draw in worldspace
}

bool ESPWindow::WorldToOrthoScreen(glmatrixf& mvpMat, float virtW, float virtH, vec* worldCoords, vec& outScreenCoords)
{
    // credit: a post on unity's forum, i think. can't remember.
    vec4 clipCoords(0.f, 0.f, 0.f, 1.f);

    mvpMat.transform(*worldCoords, clipCoords);
	/*
	clipCoords.x = worldCoords.x * mvpMat[0] + worldCoords.y * mvpMat[4] + worldCoords.z * mvpMat[8] + mvpMat[12];
	clipCoords.y = worldCoords.x * mvpMat[1] + worldCoords.y * mvpMat[5] + worldCoords.z * mvpMat[9] + mvpMat[13];
	clipCoords.z = worldCoords.x * mvpMat[2] + worldCoords.y * mvpMat[6] + worldCoords.z * mvpMat[10] + mvpMat[14];
	clipCoords.w = worldCoords.x * mvpMat[3] + worldCoords.y * mvpMat[7] + worldCoords.z * mvpMat[11] + mvpMat[15];
	*/

    if (clipCoords.w < 0.1f) return false;
    vec NDCCoords;
    NDCCoords.x = clipCoords.x / clipCoords.w;
    NDCCoords.y = clipCoords.y / clipCoords.w;
    NDCCoords.z = clipCoords.z / clipCoords.w;

    outScreenCoords.x = (HalfVPWidth * NDCCoords.x) + (NDCCoords.x + HalfVPWidth);
    outScreenCoords.y = -(HalfVPHeight * NDCCoords.y) + (NDCCoords.y + HalfVPHeight);

    return true;
}

void ESPWindow::drawstring_2d(const char* str, float x, float y, vec4* color)
{
    int length = strlen(str);
    glColor4f(color->x, color->y, color->z, color->w);
    glRasterPos2f(x, y);
    glListBase(1000);
    glCallLists(length, GL_UNSIGNED_BYTE, str);
}

void ESPWindow::drawstringf_2d(const char* format, int x, int y, vec4* color, ...)
{
    ac_string d;
    va_list ap; va_start(ap, color); vformatstring(d, format, ap); va_end(ap);

    int length = strlen(d);
    glColor4f(color->x, color->y, color->z, color->w);
    glRasterPos2f(x, y);
    glListBase(1000);
    glCallLists(length, GL_UNSIGNED_BYTE, d);
}

void ESPWindow::box2d(float x1, float y1, float x2, float y2, vec4* color)
{
    glColor4f(color->x, color->y, color->z, color->w);
    glBegin(GL_TRIANGLE_STRIP);
    // tri[0] = v1,v0,v2
    // tri[1] = v2, v1, v3
    glVertex2f(x1, y1); // v0 = x1,y1
    glVertex2f(x2, y1); // v1 = x2,y1
    glVertex2f(x1, y2); // v2 = x1,y2
    glVertex2f(x2, y2); // v3 = x2,y2
    glEnd();
}

void ESPWindow::outline2d(float x1, float y1, float x2, float y2, vec4* color)
{
    glColor4f(color->x, color->y, color->z, color->w);
    glBegin(GL_LINE_LOOP);
    glVertex2f(x1, y1);
    glVertex2f(x2, y1);
    glVertex2f(x2, y2);
    glVertex2f(x1, y2);
    glEnd();
}