#pragma once

#include "ExternalMemoryReader.h"

class AssaultCubeMemoryReader : ExternalMemoryReader
{
public:
	// i wanted to do some big brain stuff where accessing any member of the XX_wrapper classes would result in an automatic ReadProcessMemory call
	// but implementing auto-caching is impossible in that case (without reading something like the framecount each time), and i can't trust people to cache values themselves (and then yell at me for laggy cheats as they make 20000 RPM calls per frame)
	// SO, this is what i've settled on:
	// you call Update() every frame and that'll read the assaultcube_wrapper struct, all players/bots, and the currently equipped weapon (pointing the guninfo member to the proper global guninfo struct)
	// since guninfo structs are global and read-only, we only need to read each one once.
	// and i just double-checked - they are laid out sequentially, so we can read all guninfo structs with just one RPM call
	// in total, it should be ~11 reads for assaultcube_wrapper, then 2 for each player/bot
	static void InitializeForProcess(HANDLE proc);
	static class assaultcube_wrapper* Update();

	static void WriteHealthAndArmor();
	static void WriteAmmo();
	static void WriteGunInfo();
	static void RestoreGunInfo();
	static void WriteYawAndPitch(float yaw, float pitch);

private:
	static bool HasRanOnce;
	static class assaultcube_wrapper* assaultcube_main;
};