#include "ExternalMemoryReader.h"
#include <Psapi.h>
#include <tchar.h>

/*static*/ DWORD ExternalMemoryReader::ProcessBaseAddress = 0;
/*static*/ HANDLE ExternalMemoryReader::ProcHandle = NULL;
/*static*/ bool ExternalMemoryReader::IsInitialized = false;

/*static*/ void ExternalMemoryReader::InitializeForProcess(HANDLE proc)
{
	ProcHandle = proc;
	TCHAR procFilename[MAX_PATH];
	DWORD numCharsInName = sizeof(procFilename) / sizeof(TCHAR);
	if (QueryFullProcessImageName(proc, 0, (LPTSTR)procFilename, &numCharsInName) == 0)
	{
		// TO-DO: 
		// error reporting
		MessageBox(NULL, L"Could not get process image name", L"Assault Cube Multihack - External", 0);
		return;
	}
	HMODULE hMods[1024];
	DWORD cbNeeded;
	if (EnumProcessModulesEx(proc, hMods, sizeof(hMods), &cbNeeded, LIST_MODULES_ALL))
	{
		int numMods = cbNeeded / sizeof(HMODULE);
		for (int index = 0; index < numMods; index++)
		{
			TCHAR szModName[MAX_PATH];
			if (GetModuleFileNameEx(proc, hMods[index], szModName, sizeof(szModName) / sizeof(TCHAR)))
			{
				if (_tcsncmp(procFilename, szModName, numCharsInName) == 0)
				{
					// supposedly the HMODULE is also the base address
					ProcessBaseAddress = (DWORD)hMods[index];
					IsInitialized = true;
					return;
				}
			}
		}
	}
	IsInitialized = false;
}

/*static*/ bool ExternalMemoryReader::IsStillRunning()
{
	DWORD exitCode = 0;
	GetExitCodeProcess(ProcHandle, &exitCode);
	return (exitCode == STILL_ACTIVE);
}

/*static*/ DWORD ExternalMemoryReader::ResolvePointerChain(int numOffsets, va_list offsets)
{
	DWORD ptr;
	SIZE_T numBytesRead = 0;
	int offset = va_arg(offsets, int);
	DWORD address = (ProcessBaseAddress + offset);
	if (ReadProcessMemory(ProcHandle, (LPCVOID)address, &ptr, sizeof(void*), &numBytesRead) == 0)
	{
		DWORD errCode = GetLastError();
		utils::ErrorMsgBoxW(errCode, NULL, L"Failed to resolve pointer chain, error code", L"Assault Cube Multihack - External (OpenGL)", MB_OK | MB_ICONERROR);
		return 0;
	}
	for (int index = 1; index < numOffsets; index++)
	{
		offset = va_arg(offsets, int);
		if (offset == 0x0) { break; }
		address = ptr + offset;
		if (ReadProcessMemory(ProcHandle, (LPCVOID)address, &ptr, sizeof(void*), &numBytesRead) == 0)
		{
			DWORD errCode = GetLastError();
			utils::ErrorMsgBoxW(errCode, NULL, L"Failed to resolve pointer chain, error code", L"Assault Cube Multihack - External (OpenGL)", MB_OK | MB_ICONERROR);
			return 0;
		}
	}
	return ptr;
}

/*static*/ DWORD ExternalMemoryReader::ResolvePointerChain(int numOffsets, ...)
{
	va_list args;
	va_start(args, numOffsets);
	return ResolvePointerChain(numOffsets, args);
	va_end(args);
}

/*static*/ bool ExternalMemoryReader::ReadDirect(DWORD address, void* buf, int length)
{
	SIZE_T numBytesRead = 0;
	BOOL result = ReadProcessMemory(ProcHandle, (LPVOID)address, buf, length, &numBytesRead);
	if (result == 0)
	{
		ZeroMemory(buf, length);
		DWORD errCode = GetLastError();
		utils::ErrorMsgBoxW(errCode, NULL, L"Failed to read direct, error code", L"Assault Cube Multihack - External (OpenGL)", MB_OK | MB_ICONERROR);

	}
	return result;
}

/*static*/ bool ExternalMemoryReader::WriteDirect(DWORD address, void* buf, int length)
{
	SIZE_T numBytesRead = 0;
	BOOL result = WriteProcessMemory(ProcHandle, (LPVOID)address, buf, length, &numBytesRead);
	if (result == 0)
	{
		DWORD errCode = GetLastError();
		utils::ErrorMsgBoxW(errCode, NULL, L"Failed to write direct, error code", L"Assault Cube Multihack - External (OpenGL)", MB_OK | MB_ICONERROR);
	}
	return result;
}