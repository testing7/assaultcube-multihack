#pragma once

#include <stdio.h>
#include <stdarg.h>

template<typename T>
struct vector
{
	T** data;
	int maxSlots;
	int count;
};