#pragma once

#include <Windows.h>

#include <vector> // <-- for ReadVecContainer
#include "utils.h"

struct external_vector
{
	DWORD arrAddress;
	int size;
	int count;
};

class ExternalMemoryReader
{
public:
	// i only know how to use static classes in c++
	// i'm not kidding.
	ExternalMemoryReader() = delete;
	ExternalMemoryReader(const ExternalMemoryReader&) = delete;
	ExternalMemoryReader& operator=(const ExternalMemoryReader& o) = delete;
	~ExternalMemoryReader() = delete;

	static DWORD ProcessBaseAddress;
	static HANDLE ProcHandle;
	static bool IsInitialized;


	static void InitializeForProcess(HANDLE proc);
	static bool IsStillRunning();
	static DWORD ResolvePointerChain(int numOffsets, va_list offsets);
	static DWORD ResolvePointerChain(int numOffsets, ...);

	static bool ReadDirect(DWORD address, void* buf, int length);
	static bool WriteDirect(DWORD address, void* buf, int length);

	// credit: abigail95 via [https://www.unknowncheats.me/forum/2205598-post7.html]
	template<class T>
	static bool ReadValue(DWORD dwAddress, T* Value)
	{
		SIZE_T numBytesRead = 0;
		BOOL result = ReadProcessMemory(ProcHandle, reinterpret_cast<LPVOID>(dwAddress), Value, sizeof(T), &numBytesRead);
		if (result == 0)
		{
			DWORD errCode = GetLastError();
			utils::ErrorMsgBoxW(errCode, NULL, L"Failed to read value, error code", L"Assault Cube Multihack - External (OpenGL)", MB_OK | MB_ICONERROR);
		}
		return result;
	}

	template<class T>
	static bool WriteValue(DWORD dwAddress, const T& Value)
	{
		SIZE_T numBytesRead = 0;
		BOOL result = WriteProcessMemory(ProcHandle, reinterpret_cast<LPVOID>(dwAddress), Value, sizeof(T), &numBytesRead);
		if (result == 0)
		{
			DWORD errCode = GetLastError();
			utils::ErrorMsgBoxW(errCode, NULL, L"Failed to write value, error code", L"Assault Cube Multihack - External (OpenGL)", MB_OK | MB_ICONERROR);
		}
		return result;
	}

	// specifically, +0 is ptr to array, +4 is count, +8 is capacity
	template<class T>
	static bool ReadVecContainer(DWORD address, std::vector<T>& buf)
	{
		external_vector vec = external_vector();
		bool result = ReadValue(address, &vec);
		if (result)
		{
			buf.resize(vec.count);
			DWORD* elementAddresses = new DWORD[vec.count];
			result = ReadDirect(vec.arrAddress, elementAddresses, vec.count * sizeof(DWORD));
			if (result)
			{
				for (int elementIndex = 0; elementIndex < vec.count; elementIndex++)
				{
					if (elementAddresses[elementIndex] == 0x0) continue;
					result = ReadDirect(elementAddresses[elementIndex], &(buf.data()[elementIndex]), sizeof(T));
					if (!result) break;
				}
			}
			delete[] elementAddresses;
		}
		if (result == 0)
		{
			DWORD errCode = GetLastError();
			utils::ErrorMsgBoxW(errCode, NULL, L"Failed to read vector<T>, error code", L"Assault Cube Multihack - External (OpenGL)", MB_OK | MB_ICONERROR);
		}
		return result;
	}
};