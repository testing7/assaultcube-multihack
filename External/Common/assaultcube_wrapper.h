#pragma once

#include <vector>

#include "EngineTools.h"
#include "EngineStructs.h"
#include "playerent_wrapper.h"

struct traceresult_s
{
	struct vec end;
	bool collided;
};

typedef void(__cdecl* tgl_drawhud)(int w, int h, int curfps, int nquads, int curvert, bool underwater, int elapsed);
typedef void(__cdecl* tdraw_text)(const char* str, int left, int top, int r, int g, int b, int a, int cursor, int maxwidth);
typedef void(__cdecl* tdraw_textf)(const char* format, int left, int top, ...);
typedef void(__cdecl* ttext_bounds)(const char* str, int& width, int& height, int maxwidth);
typedef void(__cdecl* tsetperspective)(float fovy, float aspect, float nearplane, float farplane);
typedef void(__cdecl* tTraceLine)(struct vec from, struct vec to, class dynent_wrapper* pTracer, int CheckPlayers, traceresult_s* tr, int SkipTags);

/*
#define VIRTW (double)3200
#define VIRTH (double)1800
*/

class assaultcube_wrapper
{
public:
	assaultcube_wrapper()
	{
		localplayer = new playerent_wrapper;
		players = new std::vector<playerent_wrapper>();
		maincamera = new physent_wrapper;
		bots = new std::vector<botent_wrapper>();
	}

	~assaultcube_wrapper()
	{
		delete localplayer;
		delete players;
		delete maincamera;
		delete bots;
	}
	assaultcube_wrapper(const assaultcube_wrapper&) = delete;
	assaultcube_wrapper& operator=(const assaultcube_wrapper& o) = delete;

	// 11 members, 7 reads

	// READ: sequential, starting @ [0x110C94 -> 0x110C98, no multidepth for either]
	int scr_w = 0;
	int scr_h = 0;

	// READ: not sequential w/ VIRTH, used for w2s [0x102574, no multidepth]
	int VIRTW = 0;
	// READ: not sequential w/ VIRTW, used for w2s [0xEE5E8, no multidepth]
	double VIRTH = 0;

	// READ: sequential starting @ [0x10F1C4 -> 0x10F1CC, no multidepth]
	float fov = 0.f;
	float scopeFOV = 0.f;
	float spectatorFOV = 0.f;
	
	// READ: sequential, starting @ [0x10F4F4, 0x0; 0x10F4F8, no multidepth].
	playerent_wrapper* localplayer;
	std::vector<playerent_wrapper>* players;

	// READ: @ [0x109B74, 0x0]
	physent_wrapper* maincamera;

	// READ: @ [0x110D90, no multidepth]
	std::vector<botent_wrapper>* bots;

	// READ: @ [0x4fc300, no multidepth]
	guninfo_wrapper guninfos[NUMGUNS];
	guninfo_wrapper guninfos_original[NUMGUNS];

	// READ: @ [0x501AE8 (base+101AE8), no multidepth]
	glmatrixf mvpmatrix;

	// READ: @ [base+101B28, no multidepth]
	glmatrixf mvMatrix;

	// READ: @ [base+101BB8), no multidepth]
	glmatrixf projMatrix;
};