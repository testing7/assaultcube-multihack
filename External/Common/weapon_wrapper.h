#pragma once


struct guninfo_wrapper
{
public:
	char modelname[23]; //0x0000 
	char title[42]; //0x0017 
	char _0x0041[195];
	__int16 sound; //0x0104 
	__int16 reload; //0x0106 
	__int16 reloadtime; //0x0108 
	__int16 attackdelay; //0x010A 
	__int16 damage; //0x010C 
	__int16 piercing; //0x010E 
	__int16 projspeed; //0x0110 
	__int16 part; //0x0112 
	__int16 spread; //0x0114 
	__int16 recoil; //0x0116 
	__int16 magsize; //0x0118 
	__int16 mdl_kick_rot; //0x011A 
	__int16 mdl_kick_back; //0x011C 
	__int16 recoilincrease; //0x011E 
	__int16 recoilbase; //0x0120 
	__int16 maxrecoil; //0x0122 
	__int16 recoilbackfade; //0x0124 
	__int16 pushfactor; //0x0126 
	__int8 isauto; //0x0128 

};//Size=0x0129

struct weapon_wrapper
{
    unsigned long vtableAddress;
    int type;
	// we're likely already coming from the playerent obj, so we don't need to read this struct, just the address is fine.
    class playerent_wrapper* owner;
	// remember to read this struct, we will need it
    struct guninfo_wrapper* info;
	int* ammo, * mag, * gunwait; // <-- just points back into the owner (playerent) structure
	int shots, reloading, lastaction;
};

