#include "../../Common/playerent_wrapper.h"
#include "../../Common/assaultcube_wrapper.h"
#include "ExternalMemoryReader.h"
// for DWORD
#include <Windows.h>

weapon_wrapper playerent_wrapper::ReadWeapon(class assaultcube_wrapper* game)
{
	weapon_wrapper weapon = weapon_wrapper();
	int numBytesRead = 0;
	DWORD addressRead = 0;
	ExternalMemoryReader::ReadDirect(weaponselAddress, &weapon, sizeof(weapon_wrapper));
	// TO-DO:
	// point guninfo member to proper guninfo struct
	weapon.info = &game->guninfos[weapon.type];

	return weapon;
}