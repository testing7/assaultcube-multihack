#pragma once

#include "PreprocessorOptions.h"

#include "Memory.h"

#include "MinHook.h"

#define WIN32_LEAN_AND_MEAN
#include <Windows.h>
#include <stdio.h>

class utils
{
public:
	utils() = delete;
	utils(const utils&) = delete;
	utils& operator=(const utils& o) = delete;
	~utils() = delete;

	static bool AttachDebugConsole();
	static void Eject(HMODULE hMod, bool bClearHook, bool fromOwnThread);
	static void PrintMatrix(const char* name, float mat[16], char* buf);

	static void DbgPrint(const char* fmt, ...);
};