#include "assaultcube_wrapper.h"

tgl_drawhud assaultcube_wrapper::ogl_drawhud_trampoline = 0;
tdraw_text assaultcube_wrapper::odraw_text = 0;
tdraw_textf assaultcube_wrapper::odraw_textf = 0;
ttext_bounds assaultcube_wrapper::text_bounds = 0;
tsetperspective assaultcube_wrapper::osetperspective = 0;
tTraceLine assaultcube_wrapper::oTraceLine = 0;

int* assaultcube_wrapper::scr_w = 0;
int* assaultcube_wrapper::scr_h = 0;
int* assaultcube_wrapper::VIRTW = 0;
double* assaultcube_wrapper::VIRTH = 0;
float* assaultcube_wrapper::scopeFOV = 0;
float* assaultcube_wrapper::spectatorFOV = 0;
float* assaultcube_wrapper::fov = 0;
playerent_wrapper* assaultcube_wrapper::localplayer = 0;
physent_wrapper** assaultcube_wrapper::maincamera = 0;
vector<class playerent_wrapper>* assaultcube_wrapper::players = 0;
vector<class botent_wrapper>* assaultcube_wrapper::bots = 0;

bool assaultcube_wrapper::Initialize()
{
	odraw_text = (tdraw_text)(Memory::ProcessBaseAddress + 0x1A150);
	odraw_textf = (tdraw_textf)(Memory::ProcessBaseAddress + 0x19880);
	text_bounds = (ttext_bounds)(Memory::ProcessBaseAddress + 0x19F00);
	osetperspective = (tsetperspective)(Memory::ProcessBaseAddress + 0x6AC0);
	// TO-DO:
	// make a signature for this function in particular, it's more difficult to find than others so it'll suck if the game updates
	oTraceLine = (tTraceLine)(Memory::ProcessBaseAddress + 0x8A310);

	scr_w = (int*)(Memory::ProcessBaseAddress + 0x110C94);
	scr_h = (int*)(Memory::ProcessBaseAddress + 0x110C98);
	VIRTW = (int*)(Memory::ProcessBaseAddress + 0x102574);
	VIRTH = (double*)(Memory::ProcessBaseAddress + 0xEE5E8);
	scopeFOV = (float*)(Memory::ProcessBaseAddress + 0x10F1C8);
	spectatorFOV = (float*)(Memory::ProcessBaseAddress + 0x10F1CC);
	fov = (float*)(Memory::ProcessBaseAddress + 0x10F1C4);
	localplayer = *(playerent_wrapper**)(Memory::ProcessBaseAddress + 0x10F4F4);
	maincamera = (physent_wrapper**)(Memory::ProcessBaseAddress + 0x109B74);
	players = (vector<class playerent_wrapper>*)(Memory::ProcessBaseAddress + 0x10F4F8);
	//numPlayers = (int*)(Memory::ProcessBaseAddress + 0x10F500);
	bots = (vector<class botent_wrapper>*)(Memory::ProcessBaseAddress + 0x110D90);
	//numBots = (int*)(Memory::ProcessBaseAddress + 0x110D98);
	return true;
}

void assaultcube_wrapper::draw_textf(const char* fstr, int left, int top, ...)
{
	ac_string d;
	va_list ap; va_start(ap, top); vformatstring(d, fstr, ap); va_end(ap);
	draw_text(d, left, top, 255, 0, 0);
}

void assaultcube_wrapper::draw_text(const char* str, int left, int top, int r, int g, int b, int a, int cursor, int maxwidth)
{
	__asm {
		push maxwidth
		push cursor
		push b
		push g
		push top
		push left
		push str
		mov eax, r
		call odraw_text
		add esp, 28
	}
}

void assaultcube_wrapper::TraceLine(vec from, vec to, dynent_wrapper* pTracer, int CheckPlayers, traceresult_s* tr /* passed in eax */, int SkipTags /* = false */)
{
	// okay, so, this is another function that uses a non-standard calling convention (or non-standard variant of a standard convention, who knows)
	// args are pushed onto stack in reverse order (push 'SkipTags' first, push 'from' last)
	// the 'tr' arg is *not* pushed, and is instead passed in EAX
	// we handle the tr arg differently, normally it's lea but since this function is basically a wrapper, it's already a ptr so we just mov the ptr into eax
	// the reason the game uses lea is because the 'tr' instance is declared within the calling function (as a stack value), so to get a pointer to it lea is used
	// for us, 'tr' is not an instance of this function, it's passed into this function as a pointer, so we can just mov it into eax.
	__asm {
		push SkipTags
		push CheckPlayers
		push pTracer
		push to.z
		push to.y
		push to.x
		push from.z
		push from.y
		push from.x
		mov eax, tr
		call oTraceLine
		add esp, 36
	}
	return;
}