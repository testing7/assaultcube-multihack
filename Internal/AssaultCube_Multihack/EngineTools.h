#pragma once

#include <stdio.h>
#include <stdarg.h>

#define MAXSTRLEN 260
typedef char ac_string[MAXSTRLEN];

inline void vformatstring(char* d, const char* fmt, va_list v, int len = MAXSTRLEN) { vsnprintf(d, len, fmt, v); d[len - 1] = 0; }

template<typename T>
struct vector
{
	T** data;
	int maxSlots;
	int count;
};