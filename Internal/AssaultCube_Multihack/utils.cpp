#include "utils.h"


void utils::DbgPrint(const char* fmt, ...)
{
#ifdef DEBUGPRINT
	va_list args;
	va_start(args, fmt);
	vprintf(fmt, args);
	va_end(args);
#endif
	return;
}

void utils::PrintMatrix(const char* name, float mat[16], char* buf)
{
	int j = sprintf_s(buf, 260, "=== Printing %s ===", name);
	for (int pass = 0; pass < 4; pass++)
		j += sprintf_s(buf + j, 260 - j, "\n\t%.2f, %.2f, %.2f, %.2f", mat[(pass * 4)], mat[(pass * 4) + 1], mat[(pass * 4) + 2], mat[(pass * 4) + 3]);
	j += sprintf_s(buf + j, 260 - j, "\n=== DONE ===\n");
}

bool utils::AttachDebugConsole(void)
{
	if (FreeConsole())
	{
		if (!AllocConsole())
			return false;
	}
	else if (!AllocConsole())
		return false;

	FILE* pConOutput = nullptr;
	return (freopen_s(&pConOutput, "CONOUT$", "w", stdout) == 0);
}

void utils::Eject(HMODULE hMod, bool bClearHook, bool fromOwnThread)
{
	// NOTE:
	// this will still crash if ANY thread is currently executing this library's code.
	// we'd need to freeze every thread but the current thread and set their instruction ptr to somewhere outside of our library
	// but, that might crash as well since state wouldn't be handled (registers and stack).
	// i think the best way would be to constantly monitor each thread's IP and wait until it's outside, then IMMEDIATELY FREEZE IT
	// and do this until all threads are frozen (except the current one), and then unhook
	// and then after unhooking, unfreeze each thread and finally call FreeLibraryAndExitThread.
	if (bClearHook)
	{
		DWORD gl_drawhudAddress = Memory::ProcessBaseAddress + 0xAAF0;
		MH_STATUS status = MH_RemoveHook((LPVOID)gl_drawhudAddress);
		if (status != MH_OK)
		{
			DbgPrint("Could not unhook gl_drawhud (status: %d)\n", (int)status);
		}
		status = MH_Uninitialize();
		if (status != MH_OK)
		{
			DbgPrint("Could not clean up MinHook (status: %d)\n", (int)status);
		}
	}
	DbgPrint("Ejecting... (module: %llx)(thread? %d)\n", hMod, (int)fromOwnThread);
#ifdef DEBUGPRINT
	FreeConsole();
#endif
	Sleep(3000);
	::Beep(400, 250);
	if (fromOwnThread)
		FreeLibraryAndExitThread(hMod, 0);
	else
		FreeLibrary(hMod);
}