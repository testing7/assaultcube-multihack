#include "opengl_wrapper.h"

tglEnable opengl_wrapper::oglEnable = NULL;
tglLineWidth opengl_wrapper::oglLineWidth = NULL;
tglDisable opengl_wrapper::oglDisable = NULL;
tglMatrixMode opengl_wrapper::oglMatrixMode = NULL;
tglLoadIdentity opengl_wrapper::oglLoadIdentity = NULL;
tglOrtho opengl_wrapper::oglOrtho = NULL;
// w2s
tglFrustum opengl_wrapper::oglFrustum = NULL;
tglGetFloatv opengl_wrapper::oglGetFloatv = NULL;
tglPushMatrix opengl_wrapper::oglPushMatrix = NULL;
tglPopMatrix opengl_wrapper::oglPopMatrix = NULL;
tglBlendFunc opengl_wrapper::oglBlendFunc = NULL;
tglTranslatef opengl_wrapper::oglTranslatef = NULL;
tglRotatef opengl_wrapper::oglRotatef = NULL;
tglScalef opengl_wrapper::oglScalef = NULL;
tglColor3f opengl_wrapper::oglColor3f = NULL;
tglColor4f opengl_wrapper::oglColor4f = NULL;
tglBegin opengl_wrapper::oglBegin = NULL;
tglVertex3f opengl_wrapper::oglVertex3f = NULL;
tglVertex2f opengl_wrapper::oglVertex2f = NULL;
tglEnd opengl_wrapper::oglEnd = NULL;


bool opengl_wrapper::Initialize()
{
	HMODULE openGLBase = GetModuleHandleA("OPENGL32.dll");
	if (openGLBase == NULL)
	{
		// TO-DO:
		// print error
		return false;
	}
	oglEnable = (tglEnable)GetProcAddress(openGLBase, "glEnable");
	if (oglEnable == NULL)
	{
		// TO-DO:
		// print error
		return false;
	}
	oglLineWidth = (tglLineWidth)GetProcAddress(openGLBase, "glLineWidth");
	if (oglLineWidth == NULL)
	{
		// TO-DO:
		// print error
		return false;
	}
	oglDisable = (tglDisable)GetProcAddress(openGLBase, "glDisable");
	if (oglDisable == NULL)
	{
		// TO-DO:
		// print error
		return false;
	}
	oglMatrixMode = (tglMatrixMode)GetProcAddress(openGLBase, "glMatrixMode");
	if (oglMatrixMode == NULL)
	{
		// TO-DO:
		// print error
		return false;
	}
	oglLoadIdentity = (tglLoadIdentity)GetProcAddress(openGLBase, "glLoadIdentity");
	if (oglLoadIdentity == NULL)
	{
		// TO-DO:
		// print error
		return false;
	}
	oglOrtho = (tglOrtho)GetProcAddress(openGLBase, "glOrtho");
	if (oglOrtho == NULL)
	{
		// TO-DO:
		// print error
		return false;
	}
	// ===
	// w2s
	// ===
	oglFrustum = (tglFrustum)GetProcAddress(openGLBase, "glFrustum");
	if (oglFrustum == NULL)
	{
		// TO-DO:
		// print error
		return false;
	}
	oglGetFloatv = (tglGetFloatv)GetProcAddress(openGLBase, "glGetFloatv");
	if (oglGetFloatv == NULL)
	{
		// TO-DO:
		// print error
		return false;
	}
	oglPushMatrix = (tglPushMatrix)GetProcAddress(openGLBase, "glPushMatrix");
	if (oglPushMatrix == NULL)
	{
		// TO-DO:
		// print error
		return false;
	}
	oglPopMatrix = (tglPopMatrix)GetProcAddress(openGLBase, "glPopMatrix");
	if (oglPopMatrix == NULL)
	{
		// TO-DO:
		// print error
		return false;
	}
	oglBlendFunc = (tglBlendFunc)GetProcAddress(openGLBase, "glBlendFunc");
	if (oglBlendFunc == NULL)
	{
		// TO-DO:
		// print error
		return false;
	}
	oglTranslatef = (tglTranslatef)GetProcAddress(openGLBase, "glTranslatef");
	if (oglTranslatef == NULL)
	{
		// TO-DO:
		// print error
		return false;
	}
	oglRotatef = (tglRotatef)GetProcAddress(openGLBase, "glRotatef");
	if (oglRotatef == NULL)
	{
		// TO-DO:
		// print error
		return false;
	}
	oglScalef = (tglScalef)GetProcAddress(openGLBase, "glScalef");
	if (oglScalef == NULL)
	{
		// TO-DO:
		// print error
		return false;
	}
	oglColor3f = (tglColor3f)GetProcAddress(openGLBase, "glColor3f");
	if (oglColor3f == NULL)
	{
		// TO-DO:
		// print error
		return false;
	}
	oglColor4f = (tglColor4f)GetProcAddress(openGLBase, "glColor4f");
	if (oglColor4f == NULL)
	{
		// TO-DO:
		// print error
		return false;
	}
	oglBegin = (tglBegin)GetProcAddress(openGLBase, "glBegin");
	if (oglBegin == NULL)
	{
		// TO-DO:
		// print error
		return false;
	}
	oglVertex3f = (tglVertex3f)GetProcAddress(openGLBase, "glVertex3f");
	if (oglVertex3f == NULL)
	{
		// TO-DO:
		// print error
		return false;
	}
	oglVertex2f = (tglVertex2f)GetProcAddress(openGLBase, "glVertex2f");
	if (oglVertex2f == NULL)
	{
		// TO-DO:
		// print error
		return false;
	}
	oglEnd = (tglEnd)GetProcAddress(openGLBase, "glEnd");
	if (oglEnd == NULL)
	{
		// TO-DO:
		// print error
		return false;
	}
	return true;
}