#pragma once

#include "opengl_wrapper.h"
#include "assaultcube_wrapper.h"
#include "playerent_wrapper.h"
#include "EngineStructs.h"

class Drawing
{
public:
	Drawing() = delete;
	Drawing(const Drawing&) = delete;
	Drawing& operator=(const Drawing& o) = delete;
	~Drawing() = delete;


	static glmatrixf mvmatrix, projmatrix, mvpmatrix, invmvmatrix, invmvpmatrix;
	static vec camForward;
	// NOTE:
	// for debugging, delete later
	static bool shouldDrawRay;
	static vec rayStart, rayEnd;

	static void DrawDebugLine();
	static void Draw3dText(vec worldPos, const char* text);
	static void box(block& b, float z1, float z2, float z3, float z4);
	static void outline(float x1, float y1, float x2, float y2);
	static void line3d(float x1, float y1, float z1, float x2, float y2, float z2);

	static void readmatrices();
	static void transplayer();
	static void setperspective(float fovy, float aspect, float nearplane, float farplane);
	static void SetupHUDDrawing();
	static void SetupWorldDrawing();
};