#pragma once

#define WIN32_LEAN_AND_MEAN
#include <Windows.h>

class Memory
{
public:
	Memory() = delete;
	Memory(const Memory&) = delete;
	Memory& operator=(const Memory& o) = delete;
	~Memory() = delete;

	static HMODULE hMod;
	static DWORD ProcessBaseAddress;
};