#pragma once

#include "EngineStructs.h"
#include "EngineTools.h"
#include "weapon_wrapper.h"

class physent_wrapper
{
public:
	unsigned long* vtable;
	vec origin, vel, vel_t;
	vec deltapos, newpos;
	float yaw, pitch, roll;
	float pitchvel;
	float maxspeed;                     // cubes per second, 24 for player
	int timeinair;                      // used for fake gravity
	float radius, eyeheight, maxeyeheight, aboveeye;  // bounding box size
	bool inwater;
	bool onfloor, onladder, jumpnext, crouching, crouchedinair, trycrouch, cancollide, stuck, scoping, shoot;
	int lastjump;
	float lastjumpheight;
	int lastsplash;
	char move, strafe;
	unsigned char state, type;
	float eyeheightvel;
	int last_pos;
};

struct animstate_wrapper
{
	int anim, frame, range, basetime;
	float speed;
};

class dynent_wrapper : public physent_wrapper
{
public:
	bool k_left, k_right, k_up, k_down;         // see input code

	animstate_wrapper prev[2], current[2];              // md2's need only [0], md3's need both for the lower&upper model
	int lastanimswitchtime[2];
	void* lastmodel[2];
	int lastrendered;
};

enum { GUN_KNIFE = 0, GUN_PISTOL, GUN_CARBINE, GUN_SHOTGUN, GUN_SUBGUN, GUN_SNIPER, GUN_ASSAULT, GUN_CPISTOL, GUN_GRENADE, GUN_AKIMBO, NUMGUNS };

class playerstate_wrapper
{
public:
	unsigned long* vtable;
	int health, armour;
	int primary, nextprimary;
	int gunselect;
	bool akimbo;
	int ammo[NUMGUNS], mag[NUMGUNS], gunwait[NUMGUNS];
	int pstatshots[NUMGUNS], pstatdamage[NUMGUNS]; // pstatdamage is always zero, but it is there
};

// i learned something new! with C++ multiple inheritance, vtables along one inheritance chain get combined (logical, makes sense), but
// vtables from other inheritance chains don't get combined, and are copied to the start of that structure
// so, dynent_wrapper's inheritance chain all gets combined into one vtable at +0x0
// THEN, playerstate_wrapper's vtable is written to the start of its data in the structure [so, playerent_wrapper+offsetTo_playerstate_data+0x0]
class playerent_wrapper : public dynent_wrapper, public playerstate_wrapper
{
public:
	int curskin, nextskin[2];
	int clientnum, lastupdate, plag, ping;
	unsigned int address;
	int lifesequence;                   // sequence id for each respawn, used in damage test
	int frags, flagscore, deaths, points, tks;
	int lastaction, lastmove, lastpain, lastvoicecom, lastdeath;
	// int clientrole; <-- doesn't seem to exist? also, lastvoicecom and lastdeath don't seem to be used
	bool attacking;
	ac_string name;
	int team;
	int weaponchanging;
	int nextweapon; // weapon we switch to
	int spectatemode, followplayercn;
	int eardamagemillis;
	int unk1; // <-- is meant to be 3 floats related to roll, but is something else. has same value as lastpain. weapons do follow immediately after.

	weapon_wrapper* weapons[NUMGUNS];
	weapon_wrapper* prevweaponsel, * weaponsel, * nextweaponsel, * primweap, * nextprimweap, * lastattackweapon;

	// poshist comes after, and it does match up with source code. can add it if i ever need it, but probably not.
	// however, every field after poshist doesn't. no idea why.
};

class botent_wrapper : public playerent_wrapper
{

};