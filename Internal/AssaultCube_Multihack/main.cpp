#include "main.h"

bool shouldAimbotLock = false;
playerent_wrapper* aimbotTargetPlayer = NULL;

void DrawPlayersBoundingBox()
{
	if ((assaultcube_wrapper::localplayer == NULL) || ((*assaultcube_wrapper::maincamera) == NULL) || (assaultcube_wrapper::bots == NULL))
		return;
	int myTeam = assaultcube_wrapper::localplayer->team;
	// for making the box always face the player. not necessary.
	float camYaw = (*assaultcube_wrapper::maincamera)->yaw - 180;
	for (int index = 0; index < assaultcube_wrapper::bots->count; index++)
	{
		playerent_wrapper* player = assaultcube_wrapper::bots->data[index];
		// TO-DO:
		// make state enum. 0x0 is 'alive'.
		if ((player == NULL) || (player->state != 0x0)) continue;
		// origin is at the player's feet.
		vec originLoc = player->origin;
		opengl_wrapper::oglPushMatrix();
		opengl_wrapper::oglBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
		opengl_wrapper::oglEnable(GL_BLEND);
		opengl_wrapper::oglTranslatef(originLoc.x, originLoc.y, originLoc.z);
		// makes the box always face the player. not fully necessary, but aesthetically pleasing.
		opengl_wrapper::oglRotatef(camYaw, 0, 0, 1);
		opengl_wrapper::oglRotatef(-90, 1, 0, 0);

		if (player->team == myTeam)
			opengl_wrapper::oglColor4f(0.0f, 1.0f, 0.0f, 0.3f);
		else if (player == aimbotTargetPlayer)
			opengl_wrapper::oglColor4f(1.0f, 0.0f, 1.0f, 1.f);
		else
			opengl_wrapper::oglColor4f(1.0f, 0.0f, 0.0f, 1.f);

		Drawing::outline(-player->radius, 0, player->radius, player->eyeheight);
		opengl_wrapper::oglDisable(GL_BLEND);
		opengl_wrapper::oglPopMatrix();
	}
}

void DrawNametags()
{
	if (assaultcube_wrapper::bots == NULL) return;

	for (int index = 0; index < assaultcube_wrapper::bots->count; index++)
	{
		playerent_wrapper* player = assaultcube_wrapper::bots->data[index];
		if ((player == NULL) || (player->state != 0x0)) continue;
		// W2S is math, i suck at math. i google ferociously. some links i always end up using:
		// https://www.scratchapixel.com/lessons/3d-basic-rendering/computing-pixel-coordinates-of-3d-point/mathematics-computing-2d-coordinates-of-3d-points
		// https://www.3dgep.com/understanding-the-view-matrix/#Transformations
		// https://answers.unity.com/questions/1014337/calculation-behind-cameraworldtoscreenpoint.html
		// https://www.codeproject.com/Articles/42848/A-New-Perspective-on-Viewing
		// credits to the appropriate authors, and probably others i'm missing. again, i just google wildly and nab everything i see until it works.
		vec4 clipCoords(0.f, 0.f, 0.f, 1.f);
		Drawing::mvpmatrix.transform(player->origin, clipCoords);
		if (clipCoords.w < 0.1f) continue;
		vec NDCCoords;
		NDCCoords.x = clipCoords.x / clipCoords.w;
		NDCCoords.y = clipCoords.y / clipCoords.w;
		NDCCoords.z = clipCoords.z / clipCoords.w;
		vec screenPos;
		// actual width is VIRTW * 2 and VIRTH * 2, so we can just skip the divide by 2.
		screenPos.x = (*assaultcube_wrapper::VIRTW * NDCCoords.x) + (NDCCoords.x + *assaultcube_wrapper::VIRTW);
		screenPos.y = -(*assaultcube_wrapper::VIRTH * NDCCoords.y) + (NDCCoords.y + *assaultcube_wrapper::VIRTH);
		assaultcube_wrapper::draw_text(player->name, screenPos.x, screenPos.y);
	}
}

void DrawFrame()
{
	// HUD drawing is simply an orthogonal projection instead of a perspective projection. coordinates correlate directly to the screen pixel position.
	// or, they would, but assault cube uses a virtual coordinate system (via VIRTW and VIRTH), so we have to take that into account too.
	// so, smaller numbers = left of screen
	// 0 is far left, VIRTW is middle of screen, VIRTW * 2 is far right (won't be rendered, but (VIRTW * 2 - 1) will be)
	Drawing::SetupHUDDrawing();

	// these values are just to anchor my messages to a common point
	int left = (*assaultcube_wrapper::VIRTW) - 500;
	// 0 is top, VIRTH is middle of screen
	int top = (*assaultcube_wrapper::VIRTH * 5 / 8) * 2;
	assaultcube_wrapper::draw_textf("hello world [%x]", left, top, Memory::hMod);
	vec playerPos = assaultcube_wrapper::localplayer->origin;
	vec cameraPos = (*assaultcube_wrapper::maincamera)->origin;
	assaultcube_wrapper::draw_textf("localplayer [%x]\norigin (%.2f, %.2f, %.2f [%.2f])\nmaincamera [%x]\norigin(%.2f, %.2f, %.2f)", 10, 500, assaultcube_wrapper::localplayer, playerPos.x, playerPos.y, playerPos.z, assaultcube_wrapper::localplayer->aboveeye, assaultcube_wrapper::maincamera, cameraPos.x, cameraPos.y, cameraPos.z);

	weapon_wrapper* myWeapon = assaultcube_wrapper::localplayer->weaponsel;
	assaultcube_wrapper::draw_textf("weapon.type: %d\ninfo @ [%x]", left, top - 1000, myWeapon->type, myWeapon->info);

	if (assaultcube_wrapper::players != NULL)
	{
		assaultcube_wrapper::draw_textf("players [%x](count: %d)", 10, 900, assaultcube_wrapper::players, assaultcube_wrapper::players->count);
	}
	if ((assaultcube_wrapper::bots != NULL) && (assaultcube_wrapper::bots->data != NULL))
	{
		botent_wrapper* firstBot = assaultcube_wrapper::bots->data[0];
		assaultcube_wrapper::draw_textf("bots [%x](count: %d)\nbot[0]: %s (%x)\nbot[0]->origin: (%.2f, %.2f, %.2f)", 10, 1100, assaultcube_wrapper::bots, assaultcube_wrapper::bots->count, firstBot->name, firstBot, firstBot->origin.x, firstBot->origin.y, firstBot->origin.z);
	}
	if (aimbotTargetPlayer)
	{
		assaultcube_wrapper::draw_textf("aiming at %s\nyaw: %.2f\npitch: %.2f", left, top + 100, aimbotTargetPlayer->name, assaultcube_wrapper::localplayer->yaw, assaultcube_wrapper::localplayer->pitch);
	}

	/*
	// NOTE:
	// all of the PrintMatrix stuff here is to answer the question 'what is an mvp matrix?'.
	// can remove.
	ac_string mvMatBuf, projMatBuf;
	PrintMatrix("MVMatrix", mvmatrix.v, mvMatBuf);
	PrintMatrix("ProjMatrix", projmatrix.v, projMatBuf);
	assaultcube_wrapper::draw_text(mvMatBuf, 1000, 10);
	assaultcube_wrapper::draw_text(projMatBuf, 1000, 1000);
	*/
#ifdef DEBUGPRINT
	ac_string mvpMatBuf;
	utils::PrintMatrix("MVPMatrix", Drawing::mvpmatrix.v, mvpMatBuf);
	assaultcube_wrapper::draw_text(mvpMatBuf, 1000, 10);
#endif

	/*
	// this was here to debug my TraceLine call, keeping it to demonstrate how you can cast arbitrary rays and detect collision.
	traceresult_s tr;
	playerPos.z += assaultcube_wrapper::localplayer->eyeheight;
	vec endPos = playerPos + (camForward * 40.f);
	// need to get cam forward...
	bool collided = assaultcube_wrapper::TraceLine(playerPos, endPos, assaultcube_wrapper::localplayer, false, &tr, 0);
	assaultcube_wrapper::draw_textf("endPos: (%.2f, %.2f, %.2f)\ncollided: %d\ntr.end: (%.2f, %.2f, %.2f)\ntr.collided: %d", left, top+400, endPos.x, endPos.y, endPos.z, collided, tr.end.x, tr.end.y, tr.end.z, tr.collided);
	*/

	// we use w2s to draw the names above heads, but the drawing itself is done on the screen rather than in the world
	// can fiddle with it more to see if i can convince openGL to draw strings in worldspace, so no w2s will be necessary AND the strings will be rendered w/ perspective projection for auto resizing
	// the problem is that i'm using the game's own draw_text function which doesn't allow much flexibility. i'd have to find a text drawing lib for openGL to be able to do this.
	DrawNametags();

	// inversely, the bounding boxes are drawn in world space w/ proper perspective projection
	Drawing::SetupWorldDrawing();
	DrawPlayersBoundingBox();

	// NOTE:
	// for debugging purposes, delete later
	if (Drawing::shouldDrawRay)
		Drawing::DrawDebugLine();
}

// credit: Rake / h4nsbr1x via [https://guidedhacking.com/threads/ultimate-calcangle-thread-how-to-calculate-angle-functions.8165/#post-73276]
vec CalcAngle(vec src, vec dst)
{
	vec angle;
	float dist = src.dist(dst);

	angle.x = -atan2f(dst.x - src.x, dst.y - src.y) / PI * 180.0f + 180.0f;
	angle.y = asinf((dst.z - src.z) / dist) * Rad2Deg;
	angle.z = 0.0f;

	return angle;
}

void DoAimbot()
{
	if ((assaultcube_wrapper::localplayer == NULL) || ((*assaultcube_wrapper::maincamera) == NULL) || (assaultcube_wrapper::bots == NULL))
		return;
	playerent_wrapper* myPlayer = assaultcube_wrapper::localplayer;
	vec myPos = myPlayer->origin;
	vec myEyePos = assaultcube_wrapper::localplayer->origin;
	myEyePos.z += assaultcube_wrapper::localplayer->eyeheight;
	int myTeam = myPlayer->team;

	float closestDistance = 3.402823466e+38F;
	aimbotTargetPlayer = NULL;
	for (int index = 0; index < assaultcube_wrapper::bots->count; index++)
	{
		playerent_wrapper* otherPlayer = assaultcube_wrapper::bots->data[index];
		if (otherPlayer == NULL) continue;
		vec otherPos = otherPlayer->origin;
		otherPos.z += otherPlayer->eyeheight;
		float distance = otherPos.dist(myPos);

		traceresult_s tr = {};
		tr.end = vec(0.f, 0.f, 0.f);
		tr.collided = false;
		vec endPos = otherPos;
		assaultcube_wrapper::TraceLine(myPos, endPos, assaultcube_wrapper::localplayer, true, &tr, 0);
		
		if ((distance < closestDistance) && (tr.collided == false) && (otherPlayer->team != myTeam) && (otherPlayer->state == 0x0))
		{
			closestDistance = distance;
			aimbotTargetPlayer = otherPlayer;
		}
	}
	if (aimbotTargetPlayer)
	{
		vec aimPos = aimbotTargetPlayer->origin;
		aimPos.z += aimbotTargetPlayer->eyeheight;

		vec angle = CalcAngle(myEyePos, aimPos);
		myPlayer->yaw = angle.x;
		myPlayer->pitch = angle.y;
	}
}

void UpdateFrame()
{
	playerent_wrapper* localplayer = assaultcube_wrapper::localplayer;
	localplayer->health = 100;
	localplayer->armour = 100;

	// can optimize this, but it's not very expensive anyway
	weapon_wrapper* myWeapon = localplayer->weaponsel;

	localplayer->mag[myWeapon->type] = myWeapon->info->magsize;
	myWeapon->info->pushfactor = 0;
	myWeapon->info->recoil = 0;
	myWeapon->info->recoilbase = 0;
	myWeapon->info->maxrecoil = 0;
	myWeapon->info->spread = 0;
	//myWeapon->info->attackdelay = 0; // gun go brrrrrt. actually not a great idea. will expend entire mag in a single frame.
	myWeapon->info->attackdelay = 15;

	if (shouldAimbotLock)
		DoAimbot();

	// NOTE:
	// for debugging purposes, delete later
	if (Input::IsKeyPressed(VK_NUMPAD8, true))
	{
		localplayer->vel = Drawing::camForward;
	}
	if (Input::IsKeyPressed(VK_NUMPAD5))
	{
		Drawing::rayStart = localplayer->origin;
		Drawing::rayEnd = Drawing::rayStart + (Drawing::camForward * 40.f);
		Drawing::shouldDrawRay = true;
	}
	if (Input::IsKeyPressed(VK_NUMPAD1))
	{
		shouldAimbotLock = !shouldAimbotLock;
	}
	// press numpad 3 to check if we're already injected (you won't believe how many times i alt-tab back into the game and forget if i've already injected)
	if (Input::IsKeyPressed(VK_NUMPAD3, false))
	{
		::Beep(300, 500);
	}
	// press delete to eject (PS this may crash the game :) but if it doesn't then you can re-inject without restarting!)
	else if (Input::IsKeyPressed(VK_DELETE))
	{
		utils::Eject(Memory::hMod, true, false);
	}
}

// the heart of the cheat. this is our hook for the game's gl_drawhud function.
// it'll be executed by the game thread, which means we can access game data and call game functions without worrying about multithreading issues.
// gl_drawhud was chosen specifically because the game is in the proper state for calling draw functions
void hk_gl_drawhud(int w, int h, int curfps, int nquads, int curvert, bool underwater, int elapsed)
{
	// populate mvmatrix, projmatrix, mvpmatrix, invmvmatrix, invmvpmatrix every frame
	// could read these from the game, but i like having my own copy too
	Drawing::readmatrices();
	// we always want to see through walls, so we'll just do that here
	opengl_wrapper::oglDisable(GL_DEPTH_TEST);

	// the meat
	// i chose to split the input handling, aimbot functionality, and godmode into the UpdateFrame() and the drawing of boxes & nametags into DrawFrame()
	// just makes it a little easier to read.
	UpdateFrame();
	DrawFrame();

	opengl_wrapper::oglEnable(GL_DEPTH_TEST);
	// call original
	assaultcube_wrapper::ogl_drawhud_trampoline(w, h, curfps, nquads, curvert, underwater, elapsed);
}

// initializes minhook and the wrappers, then hooks the game's gl_drawhud function before exiting.
// if the hook succeeds, then the meat of our cheat is in hk_dl_drawhud and it is executed by the game's thread.
VOID Initialize(HMODULE hMod)
{
	Memory::hMod = hMod;
	Memory::ProcessBaseAddress = (DWORD)GetModuleHandle(NULL);

#ifdef DEBUGPRINT
	utils::AttachDebugConsole();
#endif
	utils::DbgPrint("Injected and attached debug console\n");
	MH_STATUS status = MH_Initialize();
	if (status != MH_OK)
	{
		utils::DbgPrint("Could not initialize minhook (status: %d)\n", (int)status);
		utils::Eject(Memory::hMod, false, true);
		return;
	}
	// initialize openGL wrapper (so we can call openGL functions - note we could also link against openGL, and i probably will later, but i feel like doing it this way is more educational)
	if (!opengl_wrapper::Initialize())
	{
		utils::DbgPrint("Could not wrap openGL functions, exiting now\n");
		utils::Eject(Memory::hMod, false, true);
		return;
	}
	if (!assaultcube_wrapper::Initialize())
	{
		utils::DbgPrint("Could not wrap assaultcube functions, exiting now\n");
		utils::Eject(Memory::hMod, false, true);
		return;
	}
	// BEGIN HOOKING
	DWORD gl_drawhudAddress = Memory::ProcessBaseAddress + 0xAAF0;
	utils::DbgPrint("Hooking gl_drawhud [%llx]->[%llx]\n", (LPVOID)gl_drawhudAddress, &hk_gl_drawhud);
	status = MH_CreateHookEx((LPVOID)gl_drawhudAddress, &hk_gl_drawhud, &assaultcube_wrapper::ogl_drawhud_trampoline);
	if (status != MH_OK)
	{
		utils::DbgPrint("Could not create gl_drawhud hook (status: %d)\n", (int)status);
		utils::Eject(Memory::hMod, false, true);
		return;
	}
	status = MH_EnableHook((LPVOID)gl_drawhudAddress);
	if (status != MH_OK)
	{
		utils::DbgPrint("Could not enable gl_drawhud hook (status: %d)\n", (int)status);
		utils::Eject(Memory::hMod, false, true);
		return;
	}
	// END HOOKING
}

BOOL APIENTRY DllMain( HMODULE hModule,
                       DWORD  ul_reason_for_call,
                       LPVOID lpReserved
                     )
{
    switch (ul_reason_for_call)
    {
    case DLL_PROCESS_ATTACH:
		::Beep(500, 250);
		//DisableThreadLibraryCalls(hModule);
		CreateThread(0, 0, (LPTHREAD_START_ROUTINE)Initialize, hModule, 0, 0);
		//utils::DbgPrint("Done initializing, returning true now\n");
		break;
    case DLL_THREAD_ATTACH:
    case DLL_THREAD_DETACH:
    case DLL_PROCESS_DETACH:
        break;
    }
    return TRUE;
}

