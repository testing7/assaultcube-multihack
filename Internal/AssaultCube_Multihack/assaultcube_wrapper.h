#pragma once

#include "Memory.h"
#include "EngineTools.h"
#include "EngineStructs.h"
#include "playerent_wrapper.h"

struct traceresult_s
{
	struct vec end;
	bool collided;
};

typedef void(__cdecl* tgl_drawhud)(int w, int h, int curfps, int nquads, int curvert, bool underwater, int elapsed);
typedef void(__cdecl* tdraw_text)(const char* str, int left, int top, int r, int g, int b, int a, int cursor, int maxwidth);
typedef void(__cdecl* tdraw_textf)(const char* format, int left, int top, ...);
typedef void(__cdecl* ttext_bounds)(const char* str, int& width, int& height, int maxwidth);
typedef void(__cdecl* tsetperspective)(float fovy, float aspect, float nearplane, float farplane);
typedef void(__cdecl* tTraceLine)(struct vec from, struct vec to, class dynent_wrapper* pTracer, int CheckPlayers, traceresult_s* tr, int SkipTags);

/*
#define VIRTW (double)3200
#define VIRTH (double)1800
*/

class assaultcube_wrapper
{
public:
	assaultcube_wrapper() = delete;
	assaultcube_wrapper(const assaultcube_wrapper&) = delete;
	assaultcube_wrapper& operator=(const assaultcube_wrapper& o) = delete;
	~assaultcube_wrapper() = delete;

	static bool Initialize();
	// i want default args
	static void draw_text(const char* str, int left, int top, int r = 255, int g = 255, int b = 255, int a = 255, int cursor = -1, int maxwidth = -1);
	// TO-DO: add color args
	static void draw_textf(const char* fstr, int left, int top, ...);
	static void TraceLine(struct vec from, struct vec to, class dynent_wrapper* pTracer, int CheckPlayers, traceresult_s* tr, int SkipTags = 0);

	static tgl_drawhud ogl_drawhud_trampoline;
	static ttext_bounds text_bounds;
	static tsetperspective osetperspective;
	static int* scr_w;
	static int* scr_h;
	static int* VIRTW;
	static double* VIRTH;
	static float* scopeFOV;
	static float* spectatorFOV;
	static float* fov;
	static class playerent_wrapper* localplayer;
	static class physent_wrapper** maincamera;
	// TO-DO:
	// write slim vector wrapper (data*, max, count)
	static vector<class playerent_wrapper>* players;
	static vector<class botent_wrapper>* bots;

private:
	// use the draw_text func above, default args are worth it
	static tdraw_text odraw_text;
	static tdraw_textf odraw_textf;
	static tTraceLine oTraceLine;

};
