#pragma once

#include "EngineStructs.h"

#define WIN32_LEAN_AND_MEAN
#include <Windows.h>

typedef void(__stdcall *tglEnable)(int cap);
typedef void(__stdcall* tglLineWidth)(float width);
typedef void(__stdcall *tglDisable)(int cap);
typedef void(__stdcall *tglMatrixMode)(int cap);
typedef void(__stdcall *tglLoadIdentity)(void);
typedef void(__stdcall *tglOrtho)(double left, double right, double bottom, double top, double zNear, double zFar);
typedef void(__stdcall* tglFrustum)(double left, double right, double bottom, double top, double zNear, double zFar);

typedef void(__stdcall* tglGetFloatv)(int pname, float* params);
typedef void(__stdcall* tglPushMatrix)(void);
typedef void(__stdcall* tglPopMatrix)(void);
typedef void(__stdcall* tglBlendFunc)(int sourceFactor, int destFactor);
typedef void(__stdcall* tglTranslatef)(float x, float y, float z);
typedef void(__stdcall* tglRotatef)(float angle, float x, float y, float z);
typedef void(__stdcall* tglScalef)(float x, float y, float z);
typedef void(__stdcall* tglColor3f)(float red, float green, float blue);
typedef void(__stdcall* tglColor4f)(float red, float green, float blue, float alpha);
typedef void(__stdcall* tglBegin)(int cap);
typedef void(__stdcall* tglVertex3f)(float x, float y, float z);
typedef void(__stdcall* tglVertex2f)(float x, float y);
typedef void(__stdcall* tglEnd)(void);

#define GL_DEPTH_TEST 0xb71
#define GL_MODELVIEW 0x1700
#define GL_PROJECTION 0x1701
#define GL_BLEND 0xbe2
#define GL_TRIANGLE_STRIP 0x5 // glBegin
#define GL_LINE_LOOP 0x2 // glBegin
#define GL_LINE_STRIP 0x3 // glBegin
#define GL_ONE_MINUS_SRC_ALPHA 0x303 // glBlendFunc
#define GL_SRC_ALPHA 0x302 // glBlendFunc
#define GL_ONE 0x1 // glBlendFunc
#define GL_FOG 0xb60
#define GL_CULL_FACE 0xb44
#define GL_TEXTURE_2D 0xde1
#define GL_MODELVIEW_MATRIX 0xBA6 // glGetFloatv
#define GL_PROJECTION_MATRIX 0xBA7 // glGetFloatv

class opengl_wrapper
{
public:
	opengl_wrapper() = delete;
	opengl_wrapper(const opengl_wrapper&) = delete;
	opengl_wrapper& operator=(const opengl_wrapper& o) = delete;
	~opengl_wrapper() = delete;
	// loads func addresses via GetProcAddress
	static bool Initialize();
	static tglEnable oglEnable;
	static tglLineWidth oglLineWidth;
	static tglDisable oglDisable;;
	static tglMatrixMode oglMatrixMode;
	static tglLoadIdentity oglLoadIdentity;
	static tglOrtho oglOrtho;
	// w2s
	static tglFrustum oglFrustum;
	static tglGetFloatv oglGetFloatv;
	static tglPushMatrix oglPushMatrix;
	static tglPopMatrix oglPopMatrix;
	static tglBlendFunc oglBlendFunc;
	static tglTranslatef oglTranslatef;
	static tglRotatef oglRotatef;
	static tglScalef oglScalef;
	static tglColor3f oglColor3f;
	static tglColor4f oglColor4f;
	static tglBegin oglBegin;
	static tglVertex3f oglVertex3f;
	static tglVertex2f oglVertex2f;
	static tglEnd oglEnd;

};

