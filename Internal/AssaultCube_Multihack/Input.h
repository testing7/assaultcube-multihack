#pragma once

#define WIN32_LEAN_AND_MEAN
#include <Windows.h>

class Input
{
public:
	Input() = delete;
	Input(const Input&) = delete;
	Input& operator=(const Input& o) = delete;
	~Input() = delete;

	static bool IsKeyPressed(int vKey, bool bAllowKeyHeld = false);

	static bool bKeyLock[VK_OEM_CLEAR];
	static bool bKeyDown[VK_OEM_CLEAR];
	static ULONGLONG dwKeyTime[VK_OEM_CLEAR];
};