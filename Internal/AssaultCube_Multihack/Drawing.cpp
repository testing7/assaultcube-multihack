#include "Drawing.h"

/*static*/ glmatrixf Drawing::mvmatrix, Drawing::projmatrix, Drawing::mvpmatrix, Drawing::invmvmatrix, Drawing::invmvpmatrix;
/*static*/ vec Drawing::camForward;
// NOTE:
// for debugging, delete later
/*static*/ bool Drawing::shouldDrawRay = false;
/*static*/ vec Drawing::rayStart, Drawing::rayEnd;

	void Drawing::DrawDebugLine()
	{
		line3d(rayStart.x, rayStart.y, rayStart.z, rayEnd.x, rayEnd.y, rayEnd.z);
	}

	void Drawing::Draw3dText(vec worldPos, const char* text)
	{
		opengl_wrapper::oglPushMatrix();
		opengl_wrapper::oglBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
		opengl_wrapper::oglEnable(GL_BLEND);
		opengl_wrapper::oglTranslatef(worldPos.x, worldPos.y, worldPos.z);
		opengl_wrapper::oglRotatef((*assaultcube_wrapper::maincamera)->yaw - 180, 0, 0, 1);
		opengl_wrapper::oglRotatef(-90, 1, 0, 0);
		//opengl_wrapper::oglColor3f(1.0f, 0.0f, 0.0f);
		// draw
		assaultcube_wrapper::draw_text(text, 0, 0, 255, 0, 0, 255);

		opengl_wrapper::oglDisable(GL_BLEND);
		opengl_wrapper::oglPopMatrix();
	}

	void Drawing::box(block& b, float z1, float z2, float z3, float z4)
	{
		opengl_wrapper::oglBegin(GL_TRIANGLE_STRIP);
		opengl_wrapper::oglVertex3f((float)b.x, (float)b.y, z1);
		opengl_wrapper::oglVertex3f((float)b.x + b.xs, (float)b.y, z2);
		opengl_wrapper::oglVertex3f((float)b.x, (float)b.y + b.ys, z4);
		opengl_wrapper::oglVertex3f((float)b.x + b.xs, (float)b.y + b.ys, z3);
		opengl_wrapper::oglEnd();
	}

	void Drawing::outline(float x1, float y1, float x2, float y2)
	{
		opengl_wrapper::oglBegin(GL_LINE_LOOP);
		opengl_wrapper::oglVertex2f(x1, y1);
		opengl_wrapper::oglVertex2f(x2, y1);
		opengl_wrapper::oglVertex2f(x2, y2);
		opengl_wrapper::oglVertex2f(x1, y2);
		opengl_wrapper::oglEnd();
	}

	void Drawing::line3d(float x1, float y1, float z1, float x2, float y2, float z2)
	{
		//oglLineWidth(50.f);
		// NOTE:
		// need to mess with this some more. 2d lines are tiny, and i can't think properly enough to construct cubes right now
		opengl_wrapper::oglBegin(GL_TRIANGLE_STRIP);
		opengl_wrapper::oglVertex3f(x1, y1, z1);
		opengl_wrapper::oglVertex3f(x1, y1 + 0.01f, z1);
		opengl_wrapper::oglVertex3f(x2, y2, z2);
		opengl_wrapper::oglVertex3f(x2, y2 + 0.01f, z2);
		opengl_wrapper::oglEnd();
		//oglLineWidth(1.f);
	}

	void Drawing::readmatrices()
	{
		opengl_wrapper::oglGetFloatv(GL_MODELVIEW_MATRIX, mvmatrix.v);
		opengl_wrapper::oglGetFloatv(GL_PROJECTION_MATRIX, projmatrix.v);

		mvpmatrix.mul(projmatrix, mvmatrix);
		invmvmatrix.invert(mvmatrix);
		invmvpmatrix.invert(mvpmatrix);

		// 8, 9, 10 doesn't work
		camForward.x = mvmatrix[2] * -1.f;
		camForward.y = mvmatrix[6] * -1.f;
		camForward.z = mvmatrix[10] * -1.f;
	}

	void Drawing::transplayer()
	{
		opengl_wrapper::oglLoadIdentity();

		opengl_wrapper::oglRotatef((*assaultcube_wrapper::maincamera)->roll, 0, 0, 1);
		opengl_wrapper::oglRotatef((*assaultcube_wrapper::maincamera)->pitch, -1, 0, 0);
		opengl_wrapper::oglRotatef((*assaultcube_wrapper::maincamera)->yaw, 0, 1, 0);

		// move from RH to Z-up LH quake style worldspace
		opengl_wrapper::oglRotatef(-90, 1, 0, 0);
		opengl_wrapper::oglScalef(1, -1, 1);

		opengl_wrapper::oglTranslatef(-(*assaultcube_wrapper::maincamera)->origin.x, -(*assaultcube_wrapper::maincamera)->origin.y, -(*assaultcube_wrapper::maincamera)->origin.z);
	}

	void Drawing::setperspective(float fovy, float aspect, float nearplane, float farplane)
	{
		double ydist = nearplane * tan((double)fovy / 2 * RAD);
		double xdist = ydist * aspect;
		opengl_wrapper::oglFrustum(-xdist, xdist, -ydist, ydist, nearplane, farplane);
	}

	void Drawing::SetupHUDDrawing()
	{
		// clear all the matrices
		opengl_wrapper::oglMatrixMode(GL_MODELVIEW);
		opengl_wrapper::oglLoadIdentity();
		opengl_wrapper::oglMatrixMode(GL_PROJECTION);
		opengl_wrapper::oglLoadIdentity();

		// create ortho perspective for drawing HUD text
		opengl_wrapper::oglOrtho(0, (double)*assaultcube_wrapper::VIRTW * 2, *assaultcube_wrapper::VIRTH * 2, 0, -1, 1);
		opengl_wrapper::oglEnable(GL_TEXTURE_2D);
		// now we can draw in screenspace
	}

	void Drawing::SetupWorldDrawing()
	{
		// reconstruct our 3d projection matrix
		opengl_wrapper::oglMatrixMode(GL_PROJECTION);
		opengl_wrapper::oglLoadIdentity();
		float fov = *assaultcube_wrapper::fov;
		float aspect = float(*assaultcube_wrapper::scr_w) / *assaultcube_wrapper::scr_h;
		float fovy = 2 * atan2(tan((double)fov / 2 * RAD), aspect) / RAD;
		setperspective(fovy, aspect, 0.15f, 450.f);

		// set up our mv matrix
		opengl_wrapper::oglMatrixMode(GL_MODELVIEW);
		transplayer();

		opengl_wrapper::oglDisable(GL_TEXTURE_2D);
		// now we can draw in worldspace
	}