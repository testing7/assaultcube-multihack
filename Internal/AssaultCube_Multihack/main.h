#pragma once

#include "PreprocessorOptions.h"

#include "Input.h"
#include "Memory.h"
#include "utils.h"
#include "opengl_wrapper.h"
#include "assaultcube_wrapper.h"
#include "playerent_wrapper.h"
#include "weapon_wrapper.h"
#include "EngineStructs.h"
#include "EngineTools.h"

#include "Drawing.h"

// BEGIN minhook
#include "MinHook.h"

template <typename T>
inline MH_STATUS MH_CreateHookEx(LPVOID pTarget, LPVOID pDetour, T** ppOriginal)
{
	return MH_CreateHook(pTarget, pDetour, reinterpret_cast<LPVOID*>(ppOriginal));
}
// END minhook

#define WIN32_LEAN_AND_MEAN
#include <windows.h>

#include <stdio.h>